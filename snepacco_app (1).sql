-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 27, 2019 at 10:05 AM
-- Server version: 5.7.28
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snepacco_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_activity`
--

CREATE TABLE `log_activity` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `activity_name` varchar(150) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_group`
--

CREATE TABLE `m_group` (
  `id_group` int(11) NOT NULL,
  `nama_group` varchar(100) NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_group`
--

INSERT INTO `m_group` (`id_group`, `nama_group`, `keterangan`) VALUES
(8, 'Group A', '-');

-- --------------------------------------------------------

--
-- Table structure for table `m_kegiatan`
--

CREATE TABLE `m_kegiatan` (
  `id_kegiatan` int(5) NOT NULL,
  `id_group` int(10) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `ket_kegiatan` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kegiatan`
--

INSERT INTO `m_kegiatan` (`id_kegiatan`, `id_group`, `nama_kegiatan`, `ket_kegiatan`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(17, 2, 'Minum sambil borak', 'Minum sambil borak', NULL, NULL, NULL, NULL),
(19, 6, 'Makan makan', '1', NULL, NULL, NULL, NULL),
(20, 6, 'Nongkrong', '3', NULL, NULL, NULL, NULL),
(24, 7, 'Zikir', '1', NULL, NULL, NULL, NULL),
(25, 8, 'Jasa Pengecatan Kapal', '-', NULL, NULL, NULL, NULL),
(26, 8, 'Jasa Pengiriman Barang', '-', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_lokasi`
--

CREATE TABLE `m_lokasi` (
  `id_lokasi` int(5) NOT NULL,
  `id_perusahaan` int(5) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `ket_lokasi` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_lokasi`
--

INSERT INTO `m_lokasi` (`id_lokasi`, `id_perusahaan`, `nama_lokasi`, `ket_lokasi`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(8, 2, 'Batam', 'batam', '2019-12-02 02:09:10', 1, '2019-12-02 08:18:06', 1),
(9, 2, 'Medan', 'kota medan', '2019-12-02 05:12:26', 1, '2019-12-02 08:18:02', 1),
(10, 2, 'Jakarta', 'Jakarta', '2019-12-02 05:49:06', 1, '2019-12-02 08:17:59', 1),
(15, 3, 'Tanjung Uncang', 'Tanjung Uncang', '2019-12-04 05:23:07', 1, '2019-12-04 05:23:19', 1),
(16, 10, 'Stungkal', 'Stungkal', '2019-12-06 02:43:18', 1, '2019-12-06 02:43:18', 1),
(18, 3, 'Stungkal', 'Stungkal\r\n', '2019-12-06 02:43:54', 1, '2019-12-06 02:43:54', 1),
(20, 1, 'Riau Pekanbaru', 'Riau Pekanbaru', '2019-12-07 12:55:06', 1, '2019-12-07 12:55:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_perusahaan`
--

CREATE TABLE `m_perusahaan` (
  `id_perusahaan` int(5) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `nama_owner` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(10) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `nama_kapal` varchar(255) DEFAULT NULL,
  `ket_perusahaan` text,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_perusahaan`
--

INSERT INTO `m_perusahaan` (`id_perusahaan`, `nama_perusahaan`, `nama_owner`, `kode_perusahaan`, `alamat`, `nama_kapal`, `ket_perusahaan`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'PT. Jati Diri Makmur', 'Mr. Jati', 'JDM', 'Komp. Pelabuhan Sekupang', 'Mv. Oceana', 'Batam', '2019-11-30 14:21:52', 1, '2019-12-17 22:43:40', 1),
(2, 'PT. Cakrawala Mandiri Sejahtera', 'Julianto', 'PU', 'Jl. Batuampar No. 02', 'Kapal Samudra', 'Malaysia', '2019-11-30 14:22:15', 1, '2019-12-17 22:46:08', 1),
(3, 'PT. Andy Technology', 'Andi Jaya', 'PA', 'Jl. Batuampar No. 01', 'Ship Star', 'Indonesia', '2019-11-30 20:16:00', 1, '2019-12-26 15:32:26', 1),
(10, 'PT. Putra Alif Muhammad', 'Mr. Putra', 'PAM', 'Los Angeles', 'Mv. Vessel Ship', '-', '2019-12-02 08:56:33', 1, '2019-12-17 22:45:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_commercial`
--

CREATE TABLE `t_commercial` (
  `id_commercial` int(5) NOT NULL,
  `pda_no` varchar(30) NOT NULL,
  `id_perusahaan` int(10) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `id_lokasi` int(10) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `mata_uang` varchar(5) NOT NULL,
  `keterangan` text NOT NULL,
  `status` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='PDA No adalah Dokumen Commercial, yang terbuat secara otomatis ketika simpan form commercial.\r\n\r\nCth : 025/DW/PDA/2019\r\n[auto_increment]/[kode_perusahaan]/[PDA]/[current_year]';

--
-- Dumping data for table `t_commercial`
--

INSERT INTO `t_commercial` (`id_commercial`, `pda_no`, `id_perusahaan`, `nama_perusahaan`, `id_lokasi`, `nama_lokasi`, `tanggal`, `mata_uang`, `keterangan`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(244, 'JDM', 1, 'PT. Jati Diri Makmur', 20, 'Riau Pekanbaru', '2019-12-26 00:00:00', '', '', 0, '2019-12-26 22:35:00', 1, '2019-12-26 22:35:00', 1),
(245, 'JDM', 1, 'PT. Jati Diri Makmur', 20, 'Riau Pekanbaru', '2019-12-26 00:00:00', '', '', 0, '2019-12-26 22:36:19', 1, '2019-12-26 22:36:19', 1),
(246, 'JDM', 1, 'PT. Jati Diri Makmur', 20, 'Riau Pekanbaru', '2019-12-26 00:00:00', '', '', 0, '2019-12-27 08:22:47', NULL, '2019-12-27 08:22:47', NULL),
(248, 'JDM', 1, 'PT. Jati Diri Makmur', 20, 'Riau Pekanbaru', '2019-12-27 00:00:00', 'IDR', '', 0, '2019-12-27 08:33:33', 1, '2019-12-27 08:33:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_commercial_kegiatan`
--

CREATE TABLE `t_commercial_kegiatan` (
  `id_commercial_kegiatan` int(11) NOT NULL,
  `id_commercial` int(11) DEFAULT NULL,
  `id_kegiatan` int(11) DEFAULT NULL,
  `nama_kegiatan` varchar(255) DEFAULT NULL,
  `unit` int(5) DEFAULT NULL,
  `harga` decimal(12,2) DEFAULT NULL,
  `at_cost` decimal(12,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_commercial_kegiatan`
--

INSERT INTO `t_commercial_kegiatan` (`id_commercial_kegiatan`, `id_commercial`, `id_kegiatan`, `nama_kegiatan`, `unit`, `harga`, `at_cost`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(27, 248, 26, 'Jasa Pengiriman Barang', 5, 200000.00, 5.00, '2019-12-27 08:33:33', 1, '2019-12-27 08:33:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_invoice`
--

CREATE TABLE `t_invoice` (
  `id` int(5) NOT NULL,
  `no_invoice` varchar(100) NOT NULL,
  `id_perusahaan` int(5) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `id_lokasi` int(5) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `id_kegiatan` int(5) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `mata_uang` varchar(15) DEFAULT NULL,
  `harga` varchar(20) DEFAULT NULL,
  `tanggal_kirim` datetime DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `ket` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` enum('0','1','2','3','4','5') NOT NULL,
  `last_login` datetime NOT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `last_login`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', '2019-11-25 18:48:30', 'admin'),
(2, 'diki', '21232f297a57a5a743894a0e4a801fc3', '2', '0000-00-00 00:00:00', 'staff commercial'),
(3, 'dika', '21232f297a57a5a743894a0e4a801fc3', '3', '0000-00-00 00:00:00', 'manager commercial'),
(4, 'dike', '21232f297a57a5a743894a0e4a801fc3', '4', '0000-00-00 00:00:00', 'staff finance'),
(5, 'diko', '21232f297a57a5a743894a0e4a801fc3', '5', '0000-00-00 00:00:00', 'manager finance');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_activity`
--
ALTER TABLE `log_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_group`
--
ALTER TABLE `m_group`
  ADD PRIMARY KEY (`id_group`);

--
-- Indexes for table `m_kegiatan`
--
ALTER TABLE `m_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`) USING BTREE;

--
-- Indexes for table `m_lokasi`
--
ALTER TABLE `m_lokasi`
  ADD PRIMARY KEY (`id_lokasi`) USING BTREE;

--
-- Indexes for table `m_perusahaan`
--
ALTER TABLE `m_perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`) USING BTREE;

--
-- Indexes for table `t_commercial`
--
ALTER TABLE `t_commercial`
  ADD PRIMARY KEY (`id_commercial`) USING BTREE;

--
-- Indexes for table `t_commercial_kegiatan`
--
ALTER TABLE `t_commercial_kegiatan`
  ADD PRIMARY KEY (`id_commercial_kegiatan`),
  ADD KEY `commercial_id_fk` (`id_commercial`) USING HASH;

--
-- Indexes for table `t_invoice`
--
ALTER TABLE `t_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_activity`
--
ALTER TABLE `log_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_group`
--
ALTER TABLE `m_group`
  MODIFY `id_group` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_kegiatan`
--
ALTER TABLE `m_kegiatan`
  MODIFY `id_kegiatan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `m_lokasi`
--
ALTER TABLE `m_lokasi`
  MODIFY `id_lokasi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `m_perusahaan`
--
ALTER TABLE `m_perusahaan`
  MODIFY `id_perusahaan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `t_commercial`
--
ALTER TABLE `t_commercial`
  MODIFY `id_commercial` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT for table `t_commercial_kegiatan`
--
ALTER TABLE `t_commercial_kegiatan`
  MODIFY `id_commercial_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `t_invoice`
--
ALTER TABLE `t_invoice`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_commercial_kegiatan`
--
ALTER TABLE `t_commercial_kegiatan`
  ADD CONSTRAINT `commercial_id_fk` FOREIGN KEY (`id_commercial`) REFERENCES `t_commercial` (`id_commercial`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
