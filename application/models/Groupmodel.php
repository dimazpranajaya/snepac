<?php
class Groupmodel extends CI_Model{
  public function get_group()
  {
    $this->db->select('*');
    $this->db->from('m_group');
    // $this->db->join('m_perusahaan','m_kegiatan.id_perusahaan = m_perusahaan.id_perusahaan');
    // $this->db->join('m_lokasi','m_kegiatan.id_lokasi = m_lokasi.id_lokasi');
    $this->db->order_by('m_group.id_group', 'desc');
    return $this->db->get()->result();
  }

  public function create_group($data)
  {
    return $this->db->insert('m_group', $data);
  }
  
  public function get_id($id)
  {
    $id = array('id_group' => $id);
    return $this->db->get_where('m_group', $id);
  }

  public function update($data, $id)
  {
    $this->db->where('id_group', $id);
    $this->db->update('m_group', $data);
  }

  public function delete($id)
  { 
    $table = array('m_group','m_kegiatan');
    $this->db->where('id_group', $id);
    $this->db->delete($table);
  }

  public function get_id_list($id_group)
  {
    $this->db->select('*');
    $this->db->from('m_group');
    $this->db->join('m_kegiatan','m_group.id_group = m_kegiatan.id_group');
    // $this->db->join('m_perusahaan','m_kegiatan.id_perusahaan = m_perusahaan.id_perusahaan');
    // $this->db->join('m_lokasi','m_kegiatan.id_lokasi = m_lokasi.id_lokasi');
    $this->db->where('m_group.id_group', $id_group);
    return $this->db->get()->result();
  }
  public function select_nama($id){
    $this->db->select('nama_kegiatan');
    $this->db->from('m_kegiatan');
    $this->db->where('id_kegiatan',$id);
    return $this->db->get()->result(); 
  }
  public function create_kegiatan($data){
     return $this->db->insert('m_kegiatan', $data);
  }
  public function update_kegiatan($data, $id){
    $this->db->where('id_kegiatan', $id);
    $this->db->update('m_kegiatan', $data);
  }
  public function get_id_group($id){
    $id = array('id_kegiatan' => $id);
    return $this->db->get_where('m_kegiatan', $id);
  }
   public function delete_kegiatan($id)
  {
    $this->db->where('id_kegiatan', $id);
    $this->db->delete('m_kegiatan');
  }

  public function option_perusahaan($id_perusahaan){
    $this->db->where('id_perusahaan', $id_perusahaan);
    // $this->db->group_by('id_lokasi');
    $query = $this->db->get('m_lokasi')->result();
    // var_dump($query);exit();
    $output = '<option value="">Pilih</option>';
    foreach ($query as $key => $value) {
      $output.='<option value="'.$value->id_lokasi.'">'.$value->nama_lokasi.'</option>';
    }
    return $output;
  }

}