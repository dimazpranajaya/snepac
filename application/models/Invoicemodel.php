<?php
class Invoicemodel extends CI_Model{

	public function get_invoice()
  {
    $this->db->select('*, SUM(t_invoice_kegiatan.harga * t_invoice_kegiatan.unit) AS total_harga');
    $this->db->from('t_invoice');
    $this->db->join('t_invoice_kegiatan','t_invoice.id_invoice = t_invoice_kegiatan.id_invoice');
    // $this->db->join('m_lokasi','t_invoice.id_lokasi = m_lokasi.id_lokasi');
    // $this->db->join('m_kegiatan','t_invoice.id_kegiatan = m_kegiatan.id_kegiatan');
    // $this->db->where_in('t_invoice.status', ['0','1']);
    $this->db->group_by('t_invoice.id_invoice');
    $this->db->order_by('t_invoice.id_invoice', 'desc');
    return $this->db->get()->result();
  }

  public function get_invoice_manager()
  {
    $this->db->select('*, SUM(t_commercial_kegiatan.harga) AS total_harga');
    $this->db->from('t_commercial');
    $this->db->join('t_commercial_kegiatan','t_commercial.id_commercial = t_commercial_kegiatan.id_commercial');
    // $this->db->join('m_lokasi','t_commercial.id_lokasi = m_lokasi.id_lokasi');
    // $this->db->join('m_kegiatan','t_commercial.id_kegiatan = m_kegiatan.id_kegiatan');
    $this->db->where_in('t_commercial.status', ['0']);
    $this->db->group_by('t_commercial.id_commercial');
    $this->db->order_by('t_commercial.id_commercial', 'desc');
    return $this->db->get()->result();
  }

  public function get_invoice_staff()
  {
    $this->db->select('*, SUM(t_commercial_kegiatan.harga) AS total_harga');
    $this->db->from('t_commercial');
    $this->db->join('t_commercial_kegiatan','t_commercial.id_commercial = t_commercial_kegiatan.id_commercial');
    // $this->db->join('m_lokasi','t_commercial.id_lokasi = m_lokasi.id_lokasi');
    // $this->db->join('m_kegiatan','t_commercial.id_kegiatan = m_kegiatan.id_kegiatan');
    $this->db->where_in('t_commercial.status', ['1']);
    $this->db->group_by('t_commercial.id_commercial');
    $this->db->order_by('t_commercial.id_commercial', 'desc');
    return $this->db->get()->result();
  }

  public function create_invoice_kegiatan($data)
  {
    return $this->db->insert('t_invoice_kegiatan' , $data);  
  }

  public function create_invoice($data)
  {
    if ($this->db->insert('t_invoice', $data)) {
      return $this->db->insert_id();
    }
    return true;  
  }

   public function get_last_id_invoice()
  {
    $this->db->select_max('id_invoice');
    $this->db->from('t_invoice');
    $this->db->order_by('t_invoice.id_invoice', 'desc');
    return $this->db->get()->result();
  }

  public function select_commercial_by_pdo_no($id_commercial){
    // var_dump($id_commercial);exit();
    $this->db->select('*');
    $this->db->from('t_commercial');
    $this->db->join('t_commercial_kegiatan', 't_commercial_kegiatan.id_commercial = t_commercial.id_commercial');
    $this->db->where('t_commercial_kegiatan.id_commercial', $id_commercial);
    return $this->db->get()->result();
  }

  public function get_id_invoice($id){
    $id = array('t_invoice.id_invoice' => $id);
    $this->db->select('*, SUM(t_invoice_kegiatan.harga) AS total_harga');
    $this->db->join('t_invoice_kegiatan', 't_invoice_kegiatan.id_invoice = t_invoice.id_invoice' );
    $this->db->join('m_perusahaan', 'm_perusahaan.id_perusahaan = t_invoice.id_perusahaan' );
    $this->db->join('m_kapal', 'm_perusahaan.id_perusahaan = m_kapal.id_perusahaan' );
    return $this->db->get_where('t_invoice', $id);
  }
  public function get_id_invoice_kegiatan($id)
  {
    $this->db->select('*');
    $this->db->from('t_invoice');
    $this->db->join('t_invoice_kegiatan', 't_invoice_kegiatan.id_invoice = t_invoice.id_invoice');
    $this->db->where('t_invoice.id_invoice', $id);
    return $this->db->get()->result();
  }
  public function get_pda_no($pda_no){
    // var_dump($pda_no);exit();
     $hsl = $this->db->query("select * from t_commercial where pda_no='$pda_no'");
     if ($hsl->num_rows() > 0 ) {
       foreach ($hsl->result() as $data) {
          $hasil = array(
              'id_commercial' => $data->id_commercial,
              'pda_no' => $data->pda_no,
              'id_perusahaan' => $data->id_perusahaan,
              'nama_perusahaan' => $data->nama_perusahaan,
              'id_lokasi' => $data->id_lokasi,
              'nama_lokasi' => $data->nama_lokasi,
              'mata_uang' => $data->mata_uang,
              );
       }
     }
     return $hasil;
  }

  public function update_status_invoice($data, $id_invoice){
    // var_dump($id_invoice);exit();
     $this->db->where('id_invoice', $id_invoice);
     $this->db->update('t_invoice', $data);
  }
  
  public function delete_invoice_kegiatan($id_invice){
    $this->db->where('id_invoice', $id_invice);
    $this->db->delete('t_invoice_kegiatan');
  }

  public function delete_invoice($id_invoice){
      // var_dump($id_invoice);exit();
      $table = array('t_invoice', 't_invoice_kegiatan');
      $this->db->where('id_invoice', $id_invoice);
      $this->db->delete($table);
    }
 }