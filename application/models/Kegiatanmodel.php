<?php
class Kegiatanmodel extends CI_Model{
  public function get_kegiatan()
  {
    $this->db->select('*');
    $this->db->from('m_kegiatan');
    // $this->db->join('m_perusahaan','m_kegiatan.id_perusahaan = m_perusahaan.id_perusahaan');
    // $this->db->join('m_lokasi','m_kegiatan.id_lokasi = m_lokasi.id_lokasi');
    $this->db->order_by('m_kegiatan.id_kegiatan', 'desc');
    return $this->db->get()->result();
  }

  public function create_kegiatan($data)
  {
    return $this->db->insert('m_kegiatan', $data);
  }
 
  public function get_id($id)
  {
    $id = array('id_kegiatan' => $id);
    return $this->db->get_where('m_kegiatan', $id);
  }

  public function update($data, $id)
  {
    $this->db->where('id_kegiatan', $id);
    $this->db->update('m_kegiatan', $data);
  }

  public function delete($id)
  {
    $this->db->where('id_kegiatan', $id);
    $this->db->delete('m_kegiatan');
  }
}