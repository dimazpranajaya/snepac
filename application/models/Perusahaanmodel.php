<?php
class Perusahaanmodel extends CI_Model{
  public function get_perusahaan()
  {
    $this->db->select('*');
    $this->db->from('m_perusahaan');
    $this->db->order_by('id_perusahaan', 'desc');
    return $this->db->get()->result();
  }

  public function get_kapal()
  {
    $this->db->select('m_kapal.id_kapal,m_kapal.nama_kapal,m_perusahaan.id_perusahaan,m_perusahaan.nama_perusahaan');
    $this->db->from('m_kapal');
    $this->db->join('m_perusahaan','m_kapal.id_perusahaan = m_perusahaan.id_perusahaan');
    $this->db->order_by('m_kapal.nama_kapal', 'desc');
    return $this->db->get()->result();
  }
  
  public function get_kapal_perusahaan($id)
  {
    $this->db->select('m_kapal.id_kapal,m_kapal.nama_kapal,m_perusahaan.id_perusahaan,m_perusahaan.nama_perusahaan');
    $this->db->from('m_kapal');
    $this->db->join('m_perusahaan','m_kapal.id_perusahaan = m_perusahaan.id_perusahaan');
    $this->db->where('m_perusahaan.id_perusahaan', $id);
    return $this->db->get()->result();
  }
  
  public function create_perusahaan($data)
  {
    // return $this->db->insert('m_perusahaan', $data);
    if ($this->db->insert('m_perusahaan', $data)) {
      return $this->db->insert_id();
    }
    return true;  
  }
  
  public function create_perusahaan_kapal($data2)
  {
    return $this->db->insert('m_kapal' , $data2);  
  }

  public function get_id($id)
  {
    $id = array('id_perusahaan' => $id);
    return $this->db->get_where('m_perusahaan', $id);
  }
  public function get_id_kapal($id){
    $this->db->select('*');
    $this->db->from('m_perusahaan');
    $this->db->join('m_kapal','m_perusahaan.id_perusahaan = m_kapal.id_perusahaan');
    $this->db->where('m_perusahaan.id_perusahaan', $id);
    return $this->db->get()->result();
  }
  public function select_nama($id){
    $this->db->select('kode_perusahaan, nama_perusahaan');
    $this->db->from('m_perusahaan');
    $this->db->where('id_perusahaan',$id);
    return $this->db->get()->result(); 
  }
  public function select_nama_kapal($id){
    $this->db->select('id_kapal, nama_kapal');
    $this->db->from('m_kapal');
    $this->db->where('id_kapal',$id);
    return $this->db->get()->result(); 
  }
  public function update_perusahaan($data, $id)
  {
    $this->db->where('id_perusahaan', $id);
    $this->db->update('m_perusahaan', $data);
  }

  public function delete($id)
  {
    $table = array('m_perusahaan','m_kapal');
    $this->db->where('id_perusahaan', $id);
    $this->db->delete($table);
  }

  public function delete_kapal($id)
  {
    $this->db->where('id_perusahaan', $id);
    $this->db->delete('m_kapal');
  }

  public function get_perusahaan_report()
  {
    $this->db->select('*');
    $this->db->from('m_perusahaan');
    $this->db->join('m_kapal','m_perusahaan.id_perusahaan=m_kapal.id_perusahaan');
    return $this->db->get()->result();
  }
}