<?php
class Lokasimodel extends CI_Model{
  public function get_lokasi()
  {
    $this->db->select('*');
    $this->db->from('m_lokasi');
    //$this->db->join('m_perusahaan','m_lokasi.id_perusahaan = m_perusahaan.id_perusahaan');
    $this->db->order_by('m_lokasi.nama_lokasi', 'desc');
    return $this->db->get()->result();
  }

  public function create_lokasi($data)
  {
    return $this->db->insert('m_lokasi', $data);
  }
 
  public function get_id($id)
  {
    $id = array('id_lokasi' => $id);
    return $this->db->get_where('m_lokasi', $id);
  }
  public function select_nama($id){
    $this->db->select('nama_lokasi');
    $this->db->from('m_lokasi');
    $this->db->where('id_lokasi',$id);
    return $this->db->get()->result(); 
  }
  public function update($data, $id)
  {
    $this->db->where('id_lokasi', $id);
    $this->db->update('m_lokasi', $data);
  }

  public function delete($id)
  {
    $this->db->where('id_lokasi', $id);
    $this->db->delete('m_lokasi');
  }
}