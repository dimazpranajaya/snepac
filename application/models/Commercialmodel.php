<?php
class Commercialmodel extends CI_Model{
  public function get_commercial()
  {
    $this->db->select('t_commercial.id_commercial,t_commercial.pda_no,t_commercial.nama_perusahaan,t_commercial.nama_lokasi,t_commercial.nama_kapal,t_commercial.mata_uang,t_commercial.status,SUM(t_commercial_kegiatan.harga) AS total_harga, SUM(t_commercial_kegiatan.harga_dasar * t_commercial_kegiatan.unit) AS total_harga_dasar');
    $this->db->from('t_commercial');
    $this->db->join('t_commercial_kegiatan','t_commercial.id_commercial = t_commercial_kegiatan.id_commercial');
    $this->db->group_by('t_commercial.id_commercial');
    $this->db->order_by('t_commercial.id_commercial', 'desc');
    return $this->db->get()->result();
  }

  public function get_id_commercial_report(){
    $this->db->select('t_commercial.id_commercial,t_commercial.nama_perusahaan,t_commercial.nama_lokasi,t_commercial_kegiatan.nama_kegiatan,t_commercial.mata_uang,t_commercial_kegiatan.harga,t_commercial_kegiatan.harga_dasar,t_commercial_kegiatan.at_cost');
    $this->db->from('t_commercial');
    $this->db->join('t_commercial_kegiatan','t_commercial.id_commercial = t_commercial_kegiatan.id_commercial');
    $this->db->order_by('t_commercial.id_commercial', 'desc');
    return $this->db->get()->result_array();
  }


  public function get_commercial_approve()
  {
    $this->db->select('*, SUM(t_commercial_kegiatan.harga) AS total_harga');
    $this->db->from('t_commercial');
    $this->db->join('t_commercial_kegiatan','t_commercial.id_commercial = t_commercial_kegiatan.id_commercial');
    // $this->db->join('m_lokasi','t_commercial.id_lokasi = m_lokasi.id_lokasi');
    // $this->db->join('m_kegiatan','t_commercial.id_kegiatan = m_kegiatan.id_kegiatan');
    $this->db->where_in('t_commercial.status', ['1']);
    $this->db->group_by('t_commercial.id_commercial');
    $this->db->order_by('t_commercial.id_commercial', 'desc');
    return $this->db->get()->result();
  }

   public function get_last_id_commercial()
  {
    $this->db->select_max('id_commercial');
    $this->db->from('t_commercial');
    $this->db->order_by('t_commercial.id_commercial', 'desc');
    return $this->db->get()->result();
  }

  public function create_commercial($data)
  {
    if ($this->db->insert('t_commercial', $data)) {
      return $this->db->insert_id();
    }
    return true;  
  }
  
  public function create_commercial_kegiatan($data2)
  {
    return $this->db->insert('t_commercial_kegiatan' , $data2);  
  }
 
  public function get_id_commercial($id){
    $id = array('t_commercial.id_commercial' => $id);
    $this->db->select('*,t_commercial.nama_kapal,SUM(t_commercial_kegiatan.harga_dasar * t_commercial_kegiatan.unit) AS total_harga');
    $this->db->join('t_commercial_kegiatan', 't_commercial_kegiatan.id_commercial = t_commercial.id_commercial' );
    $this->db->join('m_perusahaan', 'm_perusahaan.id_perusahaan = t_commercial.id_perusahaan' );
    $this->db->join('m_kapal', 'm_perusahaan.id_perusahaan = m_kapal.id_perusahaan' );
    return $this->db->get_where('t_commercial', $id);
  }
  
  public function get_id_kegiatan($id)
  {
    $this->db->select('*');
    $this->db->from('t_commercial');
    $this->db->join('t_commercial_kegiatan', 't_commercial_kegiatan.id_commercial = t_commercial.id_commercial');
    $this->db->where('t_commercial.id_commercial', $id);
    return $this->db->get()->result();
  }

  public function update($data, $id)
  {
    $this->db->where('id_commercial', $id);
    $this->db->update('t_commercial', $data);
  }
  public function update_commercial($data, $id_commercial){
     $this->db->where('id_commercial', $id_commercial);
     $this->db->update('t_commercial', $data);
  }
  public function update_status($data, $id_commercial){
     $this->db->where('id_commercial', $id_commercial);
     $this->db->update('t_commercial', $data);
  }
  public function delete_commercial_kegiatan($id_commercial){
    // var_dump($id);exit();
    $this->db->where('id_commercial', $id_commercial);
    $this->db->delete('t_commercial_kegiatan');
  }

  public function delete_commercial($id){
    $table = array('t_commercial','t_commercial_kegiatan');
    $this->db->where('id_commercial', $id);
    $this->db->delete($table);
  }
}