<?php
class Usermodel extends CI_Model{
  public function get()
  {
    $this->db->select('*');
    $this->db->from('tb_user');
    $this->db->order_by('id', 'desc');
    return $this->db->get()->result();
  }

  public function create($data)
  {
    return $this->db->insert('tb_user', $data);
  }

  public function get_id($id)
  {
    $id = array('id' => $id);
    return $this->db->get_where('tb_user', $id);
  }

  public function update($data, $id)
  {
    $this->db->where('id', $id);
    $this->db->update('tb_user', $data);
  }

  public function delete($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('tb_user');
  }
}