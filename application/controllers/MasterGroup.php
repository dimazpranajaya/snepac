<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterGroup extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->Model('Perusahaanmodel');
		$this->load->Model('Lokasimodel');
		$this->load->Model('Groupmodel');
		$this->load->Model('Kegiatanmodel');
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		// $status = $this->session->userdata('status');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
			$data ['GroupData'] = $this->Groupmodel->get_group();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('master_group/master_group', $data);
			$this->load->view('footer');
		}
	}

	public function save_group(){
			$nama_group = $this->input->post('nama_group');
			$keterangan = $this->input->post('keterangan');
			// $created = date('Y-m-d H:i:s');
			// $created_by = $this->session->userdata('id');
			// $modified = date('Y-m-d H:i:s');
			// $modified_by = $this->session->userdata('id');

			$data = array('nama_group' => $nama_group,
							'keterangan' => $keterangan,
					);
			$this->Groupmodel->create_group($data);
			$this->session->Set_flashdata('success','success insert data');
			redirect('MasterGroup');
	}

	public function edit_group($id=null){
		$session = $this->session->userdata('id');
			if (isset($_POST['submit'])) {
				$id = $this->input->post('id_group');
				$nama_group = $this->input->post('nama_group');
				$keterangan = $this->input->post('keterangan');
				// $created = date('Y-m-d H:i:s');
				// $created_by = $this->session->userdata('id');
				// $modified = date('Y-m-d H:i:s');
				// $modified_by = $this->session->userdata('id');

				$data = array('nama_group' => $nama_group,
								'keterangan' => $keterangan,
						);
				$this->Groupmodel->update($data, $id);
				$this->session->Set_flashdata('success','success insert data');
				redirect('MasterGroup');
			}else{
				$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();
				$data ['GroupData'] = $this->Groupmodel->get_id($id)->row_array();
				// $data ['GroupDataList'] = $this->Groupmodel->get_id_list($id_group);
				// var_dump(json_encode($data ['GroupData']));exit();
				$data ['AdminData'] = $session;
				$this->load->view('header', $data);
				$this->load->view('master_group/master_group_edit', $data);
				$this->load->view('footer');
			}
	}

	public function delete_group(){
		if ($this->Groupmodel->delete($this->input->post('id'))) {
			return true;
		}else{
			return false;
		}
	}

	public function list_kegiatan($id_group = null){
		// $status = $this->session->userdata('status');
		$session = $this->session->userdata('id');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
			$data ['GroupData'] = $this->Groupmodel->get_id($id_group)->row_array();
			$data ['GroupDataList'] = $this->Groupmodel->get_id_list($id_group);
			// var_dump(json_encode($data ['GroupData']));exit();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('master_group/list_kegiatan', $data);
			$this->load->view('footer');
		}
	}

	public function save_kegiatan(){
			$id_group = $this->input->post('id_group');
			$nama_kegiatan = $this->input->post('nama_kegiatan');
			$ket_kegiatan = $this->input->post('ket_kegiatan');
			// $created = date('Y-m-d H:i:s');
			// $created_by = $this->session->userdata('id');
			// $modified = date('Y-m-d H:i:s');
			// $modified_by = $this->session->userdata('id');

			$data = array(
							'id_group' => $id_group,
							'nama_kegiatan' => $nama_kegiatan,
							'ket_kegiatan' => $ket_kegiatan,
					);
			// var_dump($data);exit();
			$this->Groupmodel->create_kegiatan($data);
			$this->session->Set_flashdata('success','success insert data');
			redirect('MasterGroup/list_kegiatan/'.$id_group);
	}

	public function edit_kegiatan($id=null){
			$session = $this->session->userdata('id');

			if (isset($_POST['submit'])) {
				$id_kegiatan = $this->input->post('id_kegiatan');
				$id_group = $this->input->post('id_group');
				$nama_kegiatan = $this->input->post('nama_kegiatan');
				$ket_kegiatan = $this->input->post('ket_kegiatan');
				// $created = date('Y-m-d H:i:s');
				// $created_by = $this->session->userdata('id');
				// $modified = date('Y-m-d H:i:s');
				// $modified_by = $this->session->userdata('id');

				$data = array(
								'id_group' => $id_group,
								'nama_kegiatan' => $nama_kegiatan,
								'ket_kegiatan' => $ket_kegiatan,
						);
				// var_dump($id_kegiatan);exit();
				$this->Groupmodel->update_kegiatan($data, $id_kegiatan);
				$this->session->Set_flashdata('update','success insert data');
				redirect('MasterGroup/list_kegiatan/'.$id_group);
			}else{
				$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
				$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
				$data ['GroupData'] = $this->Groupmodel->get_id($id)->row_array();
				$data ['GroupDataList'] = $this->Groupmodel->get_id_group($id)->row_array();
				// var_dump(json_encode($data ['GroupDataList']));exit();
				$data ['AdminData'] = $session;
				$this->load->view('header', $data);
				$this->load->view('master_group/kegiatan_edit', $data);
				$this->load->view('footer');
			}
	}

	public function delete_kegiatan(){
		if ($this->Groupmodel->delete_kegiatan($this->input->post('id'))) {
			return true;
		}else{
			return false;
		}
	}

	public function select_perusahaan(){
		var_dump($this->input->post('id_perusahaan'));exit();
		if ($this->input->post('id_perusahaan')) {
			echo $this->Groupmodel->option_perusahaan($this->input->post('id_perusahaan'));
		}
	}
	public function blank()
	{
			$this->load->view('header');
			$this->load->view('404');
			$this->load->view('footer');
	}

}
