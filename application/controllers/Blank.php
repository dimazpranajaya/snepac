<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class blank extends CI_Controller {

	public function __construct(){
		parent:: __construct();
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		$status = $this->session->userdata('status');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			// $data ['JadwalSholatData'] = $this->JadwalSholatmodel->get();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('404', $data);
			$this->load->view('footer');
		}
	}

}
