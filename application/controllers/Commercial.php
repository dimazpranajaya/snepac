<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Commercial extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->Model('Perusahaanmodel');
		$this->load->Model('Lokasimodel');
		$this->load->Model('Kegiatanmodel');
		$this->load->Model('Groupmodel');
		$this->load->Model('Commercialmodel');
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		// $status = $this->session->userdata('status');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
			$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();
			$data ['CommercialData'] = $this->Commercialmodel->get_commercial();
			$data ['AdminData'] = $session;
			// var_dump(json_encode($data ['CommercialData']));exit();
			$this->load->view('header', $data);
			$this->load->view('commercial/commercial', $data);
			$this->load->view('footer');
		}
	}

	public function save_commercial(){
		$lastCommercial = $this->Commercialmodel->get_last_id_commercial();
		$lastNoCommercial = $lastCommercial[0]->id_commercial + 1;
		$tahun = date("Y");
		//$idx = $this->input->post('id_perusahaan');
		$session = $this->session->userdata('id');

		if (isset($_POST['submit'])) {
			$id_perusahaan = $this->input->post('id_perusahaan');
			$kode_perusahaan = $this->Perusahaanmodel->select_nama($id_perusahaan);
			$nama_perusahaan = $this->Perusahaanmodel->select_nama($id_perusahaan);
			$id_lokasi = $this->input->post('id_lokasi');
			$nama_lokasi = $this->Lokasimodel->select_nama($id_lokasi);
			$id_kapal = $this->input->post('id_kapal');
			$nama_kapal = $this->Perusahaanmodel->select_nama_kapal($id_kapal);
			// var_dump('<pre>'.json_encode($nama_kapal).'</pre>');exit();
			$mata_uang = $this->input->post('mata_uang');
			$tanggal = $this->input->post('tanggal');
			$status = '0';
			$created = date('Y-m-d H:i:s');
			$created_by = $this->session->userdata('id');
			$modified = date('Y-m-d H:i:s');
			$modified_by = $this->session->userdata('id');
			$data = array( 
					'id_perusahaan' => $id_perusahaan,
					'pda_no' => $lastNoCommercial.'/'.$kode_perusahaan[0]->kode_perusahaan.'/PDA/'.$tahun,
					'nama_perusahaan' => $nama_perusahaan[0]->nama_perusahaan,
					'id_lokasi' => $id_lokasi,
					'nama_lokasi' => $nama_lokasi[0]->nama_lokasi,
					'id_kapal' => $id_kapal,
					'nama_kapal' => $nama_kapal[0]->nama_kapal,
					'tanggal' => date("Y-m-d H:i:s", strtotime($tanggal)),
					'mata_uang' => $mata_uang,
					'status' => $status,
					'created'	=> $created,
					'created_by'	=> $created_by,
					'modified'	=> $modified,
					'modified_by'	=> $modified_by,
				);
		// var_dump($data);exit();
		$this->Commercialmodel->create_commercial($data);
		$last_id = $this->db->insert_id();

		$id_kegiatan = $_POST['id_kegiatan'];
		$unit = $_POST['unit'];
		$harga = $_POST['harga'];
		$harga_dasar = $_POST['harga_dasar'];
		$at_cost = $_POST['at_cost'];
		$ket_kegiatan = $_POST['ket_kegiatan'];
		$actual = [];
		for ($i = 0; $i < count($id_kegiatan); $i++)
			$actual[] = [
				'id_commercial' => $last_id,
				'id_kegiatan'   => $id_kegiatan[$i],
				'nama_kegiatan' => $this->Groupmodel->select_nama($id_kegiatan[$i])[0]->nama_kegiatan,
				'unit'          => $unit[$i],
				'harga'         => $harga[$i],
				'harga_dasar'   => $harga_dasar[$i],
				'at_cost'       => $at_cost[$i],
				'ket_kegiatan'  => $ket_kegiatan[$i],
				'created'		=> $created,
				'created_by'	=> $created_by,
				'modified'		=> $modified,
				'modified_by'	=> $modified_by,
			];
			// var_dump('<pre>'.json_encode($actual).'</pre>');exit();
			foreach ($actual as $key => $value) {
				$this->Commercialmodel->create_commercial_kegiatan($value);
			}
			$this->session->Set_flashdata('success','success insert data');
			redirect('Commercial');
		}else{
			$data = array(
						'PerusahaanData' => $this->Perusahaanmodel->get_perusahaan(),
						'KapalData' => $this->Perusahaanmodel->get_kapal(),
						//'KapalData' => $this->Perusahaanmodel->get_kapal_perusahaan($idx),
						'LokasiData' => $this->Lokasimodel->get_lokasi(),
						'KegiatanData' => $this->Kegiatanmodel->get_kegiatan(),
						'CommercialData' => $this->Commercialmodel->get_commercial(),
						'AdminData' => $session);
			// var_dump(json_encode($data ['KapalData']));exit();
			$this->load->view('header', $data);
			$this->load->view('commercial/commercial_create', $data);
			$this->load->view('footer');
		}
	}

	public function edit_commercial($id = null){
		$session = $this->session->userdata('id');
		if (isset($_POST['submit'])) {
			$id_commercial = $this->input->post('id_commercial');
			$id_perusahaan = $this->input->post('id_perusahaan');
			$kode_perusahaan = $this->Perusahaanmodel->select_nama($id_perusahaan);
			$nama_perusahaan = $this->Perusahaanmodel->select_nama($id_perusahaan);
			$id_lokasi = $this->input->post('id_lokasi');
			$nama_lokasi = $this->Lokasimodel->select_nama($id_lokasi);
			$id_kapal = $this->input->post('id_kapal');
			$nama_kapal = $this->Perusahaanmodel->select_nama_kapal($id_kapal);
			$mata_uang = $this->input->post('mata_uang');
			$tanggal = $this->input->post('tanggal');
			$status = '0';
			$created = date('Y-m-d H:i:s');
			$created_by = $this->session->userdata('id');
			$modified = date('Y-m-d H:i:s');
			$modified_by = $this->session->userdata('id');
			$data = array( 'id_perusahaan' => $id_perusahaan,
				// 'pda_no' => $kode_perusahaan[0]->kode_perusahaan,
				'nama_perusahaan' => $nama_perusahaan[0]->nama_perusahaan,
				'id_lokasi' => $id_lokasi,
				'nama_lokasi' => $nama_lokasi[0]->nama_lokasi,
				'id_kapal' => $id_kapal,
				'nama_kapal' => $nama_kapal[0]->nama_kapal,
				'mata_uang'     => $mata_uang,		
				'tanggal' => date("Y-m-d H:i:s", strtotime($tanggal)),
				'status' => $status,
				'created'	=> $created,
				'created_by'	=> $created_by,
				'modified'	=> $modified,
				'modified_by'	=> $modified_by,
			);

			$this->Commercialmodel->update_commercial($data, $id_commercial);
			$last_id = $this->input->post('id_commercial');
		
			$this->Commercialmodel->delete_commercial_kegiatan($id_commercial);

			$id_commercial_kegiatan = $this->input->post('id_commercial_kegiatan');
			$id_kegiatan = $_POST['id_kegiatan'];
			$unit = $_POST['unit'];
			$harga = $_POST['harga'];
			$harga_dasar = $_POST['harga_dasar'];
			$at_cost = $_POST['at_cost'];
			$ket_kegiatan = $_POST['ket_kegiatan'];


			$actual = [];
			for ($i = 0; $i < count($id_kegiatan); $i++)
				$actual[] = [
					'id_commercial' => $last_id,
					'id_kegiatan'   => $id_kegiatan[$i],
					'nama_kegiatan' => $this->Groupmodel->select_nama($id_kegiatan[$i])[0]->nama_kegiatan,
					'unit'          => $unit[$i],
					'harga'         => $harga[$i],
					'harga_dasar'   => $harga_dasar[$i],
					'at_cost'       => $at_cost[$i],
					'ket_kegiatan'  => $ket_kegiatan[$i],
					'created'		=> $created,
					'created_by'	=> $created_by,
					'modified'		=> $modified,
					'modified_by'	=> $modified_by,
				];
				// var_dump('<pre>'.json_encode($actual).'</pre>');exit();
				foreach ($actual as $key => $value) {
				// var_dump('<pre>'.json_encode($actual).'</pre>');exit();
					$this->Commercialmodel->create_commercial_kegiatan($value);
				}
			// print('<pre>'.print_r($data2 , true).'</pre>');exit();
			// $this->Commercialmodel->create_commercial_kegiatan($new_data);
				$this->session->Set_flashdata('success','success insert data');
				redirect('Commercial');
			}else{
				$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
				$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
				$data ['KapalData'] = $this->Perusahaanmodel->get_kapal();
				$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();		
				$data ['CommercialData'] = $this->Commercialmodel->get_id_commercial($id)->row_array();
				$data ['CommercialKegiatan'] = $this->Commercialmodel->get_id_kegiatan($id);
			// print("<pre>".print_r($data ['CommercialData'], true)."</pre>");exit();
				$data ['AdminData'] = $session;
				$this->load->view('header', $data);
				$this->load->view('commercial/commercial_edit', $data);
				$this->load->view('footer');
			}
		}

		public function delete_commercial(){
			if ($this->Commercialmodel->delete_commercial($this->input->post('id'))) {
				return true;
			}else{
				return false;
			}
		}
		
		public function report_excel(){
			$data = array( 'title' => 'Laporan Commercial Excel',
							// 'PerusahaanData' => $this->Perusahaanmodel->get_perusahaan(),
							// 'LokasiData' => $this->Lokasimodel->get_lokasi(),
							// 'KegiatanData' => $this->Kegiatanmodel->get_kegiatan(),
							'CommercialData' => $this->Commercialmodel->get_id_commercial_report(),);
							// print_r("<pre>".print_r($data['CommercialData'], true)."</pre>");exit();
			$this->load->view('laporan_excel',$data);
		}

		public function report_pdf(){
			$data = array( 
							// 'PerusahaanData' => $this->Perusahaanmodel->get_perusahaan(),
							// 'LokasiData' => $this->Lokasimodel->get_lokasi(),
							// 'KegiatanData' => $this->Kegiatanmodel->get_kegiatan(),
							'CommercialData' => $this->Commercialmodel->get_id_commercial_report(),);
							// print_r("<pre>".print_r($data['CommercialData'], true)."</pre>");exit();
			$this->load->view('laporan_pdf', $data);

		}

		public function report_pdf_detail($id=null){
			$data = array(
					// 'PerusahaanData' => $this->Perusahaanmodel->get_perusahaan_report(),
					// 'LokasiData' => $this->Lokasimodel->get_lokasi(),
					// 'KegiatanData' => $this->Kegiatanmodel->get_kegiatan(),		
					'CommercialData' => $this->Commercialmodel->get_id_commercial($id)->row_array(),
					'CommercialKegiatan' => $this->Commercialmodel->get_id_kegiatan($id),
						);
			// var_dump(json_encode($data['CommercialData']));exit();
			$this->load->view('laporan_pdf_detail', $data);
		}

		public function approve_commercial($id = null){
			// var_dump('sss');exit();	
			$session = $this->session->userdata('id');
			if (isset($_POST['approve'])) {
				$id = $this->input->post('id_commercial');
				$status 	   = '1';
				$modified = date('Y-m-d H:i:s');
				$modified_by = $this->session->userdata('id');
				$data = array(
					'modified' 		=> $modified,
					'modified_by' 	=> $modified_by,
					'status' 		=> $status,
					);
				$this->Commercialmodel->update_status($data, $id);
				$this->session->Set_flashdata('success','success insert data');
				redirect('Commercial');
			}elseif (isset($_POST['cencel'])) {
				$id = $this->input->post('id_commercial');
				$status 	   = '2';
				$modified = date('Y-m-d H:i:s');
				$modified_by = $this->session->userdata('id');
				$data = array(
					'modified' 		=> $modified,
					'modified_by' 	=> $modified_by,
					'status' 		=> $status,
					);

				$this->Commercialmodel->update_status($data, $id);
				$this->session->Set_flashdata('success','success insert data');
				redirect('Commercial');
			}elseif(isset($_POST['approve_cancel'])) {
				$id = $this->input->post('id_commercial');
				$status 	   = '3';
				$keterangan    = $this->input->post('keterangan');
				$modified = date('Y-m-d H:i:s');
				$modified_by = $this->session->userdata('id');
				$data = array(
					'modified' 		=> $modified,
					'modified_by' 	=> $modified_by,
					'status' 		=> $status,
					'keterangan' 	=> $keterangan,
					);
				// var_dump($data);exit();
				$this->Commercialmodel->update_status($data, $id);
				$this->session->Set_flashdata('success','success insert data');
				redirect('Commercial');
			}
			
			else{
				$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
				$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
				$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();		
				$data ['CommercialData'] = $this->Commercialmodel->get_id_commercial($id)->row_array();
				$data ['CommercialKegiatan'] = $this->Commercialmodel->get_id_kegiatan($id);
			// print("<pre>".print_r($data ['CommercialKegiatan'], true)."</pre>");exit();
				$data ['AdminData'] = $session;
				$this->load->view('header', $data);
				$this->load->view('commercial/commercial_approve', $data);
				$this->load->view('footer');
			}
		}

		public function detail_commercial($id = null){
			$session = $this->session->userdata('id');
			
				// $data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
				// $data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
				// $data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();		
				$data ['CommercialData'] = $this->Commercialmodel->get_id_commercial($id)->row_array();
				$data ['CommercialKegiatan'] = $this->Commercialmodel->get_id_kegiatan($id);
			// print("<pre>".print_r($data ['CommercialData'], true)."</pre>");exit();
				$data ['AdminData'] = $session;
				$this->load->view('header', $data);
				$this->load->view('commercial/commercial_detail', $data);
				$this->load->view('footer');
		}

		public function blank()
		{
			$this->load->view('header');
			$this->load->view('404');
			$this->load->view('footer');
		}

	}
