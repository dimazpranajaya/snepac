<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterPerusahaan extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->Model('Perusahaanmodel');
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		// $status = $this->session->userdata('status');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data2 ['AdminData'] = $session;
			$this->load->view('header', $data2);
			$this->load->view('master_perusahaan/master_perusahaan', $data);
			$this->load->view('footer');
		}
	}
	public function save_perusahaan(){
			if (isset($_POST['submit'])) {
				$perusahaan = $this->input->post('perusahaan');
				$kode_perusahaan = $this->input->post('kode_perusahaan');
				$alamat = $this->input->post('alamat');
				$nama_owner = $this->input->post('nama_owner');
				$nama_kapal = $this->input->post('nama_kapal');
				$ket_perusahaan = $this->input->post('ket_perusahaan');
				$created = date('Y-m-d H:i:s');
				$created_by = $this->session->userdata('id');
				$modified = date('Y-m-d H:i:s');
				$modified_by = $this->session->userdata('id');
				
				$data = array( 'kode_perusahaan' => $kode_perusahaan,
								'nama_perusahaan' => $perusahaan,
								'nama_owner' => $nama_owner,								
								'alamat' => $alamat,
								'ket_perusahaan' => $ket_perusahaan,
								'created' => $created,
								'created_by' => $created_by,
								'modified' => $modified,
								'modified_by' => $modified_by,
						);

				$this->Perusahaanmodel->create_perusahaan($data);
				$last_id = $this->db->insert_id();
				// var_dump($last_id);exit();

				// $id_kapal = $_POST['id_kapal'];
				$nama_kapal = $_POST['nama_kapal'];
				$imo = $_POST['imo'];
				$grt = $_POST['grt'];
				$ket = $_POST['ket'];

				$actual = [];
				for ($i = 0; $i < count($nama_kapal); $i++)
					$actual[] = [
						'id_perusahaan'	=> $last_id,
						'nama_kapal'   => $nama_kapal[$i],
						'imo'          => $imo[$i],
						'grt'         => $grt[$i],
						'keterangan'  => $ket[$i],
						// 'created'		=> $created,
						// 'created_by'	=> $created_by,
						// 'modified'		=> $modified,
						// 'modified_by'	=> $modified_by,
					];
					// var_dump('<pre>'.json_encode($actual).'</pre>');exit();
					foreach ($actual as $key => $value) {
						$this->Perusahaanmodel->create_perusahaan_kapal($value);
					}
					
				$this->session->Set_flashdata('success','success insert data');
				redirect('MasterPerusahaan');
			}else{
				$session = $this->session->userdata('id');
				$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();			
				$data ['AdminData'] = $session;
				$this->load->view('header', $data);
				$this->load->view('master_perusahaan/master_perusahaan_create',$data);
				$this->load->view('footer');
			}
	}

	public function edit_perusahaan($id=null){
		$session = $this->session->userdata('id');

		if (isset($_POST['submit'])) {
			$id_perusahaan = $this->input->post('id_perusahaan');
			$perusahaan = $this->input->post('perusahaan');
			$kode_perusahaan = $this->input->post('kode_perusahaan');
			$alamat = $this->input->post('alamat');
			$nama_owner = $this->input->post('nama_owner');
			$nama_kapal = $this->input->post('nama_kapal');
			$ket_perusahaan = $this->input->post('ket_perusahaan');
			$modified = date('Y-m-d H:i:s');
			$modified_by = $this->session->userdata('id');
				$data = array( 'kode_perusahaan' => $kode_perusahaan,
								'nama_perusahaan' => $perusahaan,
								'nama_owner' => $nama_owner,								
								'alamat' => $alamat,
								'ket_perusahaan' => $ket_perusahaan,
								'modified' => $modified,
								'modified_by' => $modified_by,
						);
			$this->Perusahaanmodel->update_perusahaan($data, $id_perusahaan);
			$last_id = $this->input->post('id_perusahaan');

			$this->Perusahaanmodel->delete_kapal($id_perusahaan);
				$nama_kapal = $_POST['nama_kapal'];
				$imo = $_POST['imo'];
				$grt = $_POST['grt'];
				$ket = $_POST['ket'];

				$actual = [];
				for ($i = 0; $i < count($nama_kapal); $i++)
					$actual[] = [
						'id_perusahaan'	=> $last_id,
						'nama_kapal'   => $nama_kapal[$i],
						'imo'          => $imo[$i],
						'grt'         => $grt[$i],
						'keterangan'  => $ket[$i],
						// 'created'		=> $created,
						// 'created_by'	=> $created_by,
						// 'modified'		=> $modified,
						// 'modified_by'	=> $modified_by,
					];
					// var_dump('<pre>'.json_encode($actual).'</pre>');exit();
					foreach ($actual as $key => $value) {
						$this->Perusahaanmodel->create_perusahaan_kapal($value);
					}
					
            $this->session->set_flashdata('update', "You Success Update Data.");
           	redirect('MasterPerusahaan');
		}else{
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_id($id)->row_array();			
			$data ['KapalData'] = $this->Perusahaanmodel->get_id_kapal($id);
			// var_dump($data ['KapalData']);exit();			
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('master_perusahaan/master_perusahaan_edit',$data);
			$this->load->view('footer');
		}
	}

	public function view($id=null){
		$session = $this->session->userdata('id');
		$data ['PerusahaanData'] = $this->Perusahaanmodel->get_id($id)->row_array();			
		$data ['KapalData'] = $this->Perusahaanmodel->get_id_kapal($id);
		// var_dump($data ['PerusahaanData']);exit();
		$data ['AdminData'] = $session;
		$this->load->view('header', $data);
		$this->load->view('master_perusahaan/master_perusahaan_view',$data);
		$this->load->view('footer');
	}

	public function delete_perusahaan(){
		if ($this->Perusahaanmodel->delete($this->input->post('id'))) {
			return true;
		}else{
			return false;
		}
	}

	public function blank()
	{
			$this->load->view('header');
			$this->load->view('404');
			$this->load->view('footer');
	}

}

