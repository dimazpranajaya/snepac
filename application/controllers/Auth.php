<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('loginmodel');
	}

	function index()
	{
		$session = $this->session->userdata('id');
		$status = $this->session->userdata('id');
		if (!empty($session)) {
			redirect('Dashboard');
		}
		$this->form_validation->set_rules('username', 'Username', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login');
		}else{
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$valid = $this->loginmodel->Auth_user($username, $password);
			if ($valid !== false){
				$this->session->set_userdata(array('id' => $valid->id));
				$this->session->set_userdata(array('username' => $valid->username));
				$this->session->set_userdata(array('level' => $valid->level));
				$this->session->set_flashdata('success',"SUCCESS_MESSAGE_HERE");
				redirect('Dashboard');
			}else{
				$this->session->set_flashdata('error', 'Login error, Something wrong, Errord!!!');
				redirect('Auth');
			}
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('Auth');
	}
}