<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterLokasi extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->Model('Perusahaanmodel');
		$this->load->Model('Lokasimodel');
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		// $status = $this->session->userdata('status');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			//$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('master_lokasi/master_lokasi', $data);
			$this->load->view('footer');
		}
	}
	public function save_lokasi(){
			//$id_perusahaan = $this->input->post('id_perusahaan');
			$nama_lokasi = $this->input->post('nama_lokasi');
			$ket_lokasi = $this->input->post('ket_lokasi');
			$created = date('Y-m-d H:i:s');
			$created_by = $this->session->userdata('id');
			$modified = date('Y-m-d H:i:s');
			$modified_by = $this->session->userdata('id');

			$data = array(	//'id_perusahaan' => $id_perusahaan,
							'nama_lokasi' => $nama_lokasi,
							'ket_lokasi' => $ket_lokasi,
							'created'	=> $created,
							'created_by'	=> $created_by,
							'modified'	=> $modified,
							'modified_by'	=> $modified_by,
					);
			// var_dump($data);exit();
			$this->Lokasimodel->create_lokasi($data);
			$this->session->Set_flashdata('success','success insert data');
			redirect('MasterLokasi');
	}


	public function edit_lokasi($id=null){
		$session = $this->session->userdata('id');
		
		if (isset($_POST['submit'])) {
			$id_lokasi = $this->input->post('id_lokasi');
			// $id_perusahaan = $this->input->post('id_perusahaan');
			$nama_lokasi = $this->input->post('nama_lokasi');
			$ket_lokasi = $this->input->post('ket_lokasi');
			$modified = date('Y-m-d H:i:s');
			$modified_by = $this->session->userdata('id');

			$data = array(	//'id_perusahaan' => $id_perusahaan,
							'nama_lokasi' => $nama_lokasi,
							'ket_lokasi' => $ket_lokasi,
							'modified'	=> $modified,
							'modified_by'	=> $modified_by,
					);
			// var_dump($id_lokasi);exit();
			$this->Lokasimodel->update($data, $id_lokasi);
            $this->session->set_flashdata('update', "You Success Update Data.");
           	redirect('MasterLokasi');
		}else{
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_id($id)->row_array();			
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('master_lokasi/master_lokasi_edit',$data);
			$this->load->view('footer');
		}
	}

	public function delete_lokasi(){
		if ($this->Lokasimodel->delete($this->input->post('id'))) {
			return true;
		}else{
			return false;
		}
	}


	public function blank()
	{
			$this->load->view('header');
			$this->load->view('404');
			$this->load->view('footer');
	}

}
