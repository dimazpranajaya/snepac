<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermanagement extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('Usermanagementmodal');
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		$session = $this->session->userdata('username');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			$data ['UsermanagementData'] = $this->Usermanagementmodal->get();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('usermanagement/usermanagement', $data);
			$this->load->view('footer');
		}
	}

	public function blank()
	{
			$this->load->view('header');
			$this->load->view('404');
			$this->load->view('footer');
	}

}
