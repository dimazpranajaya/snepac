<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterKegiatan extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->Model('Perusahaanmodel');
		$this->load->Model('Lokasimodel');
		$this->load->Model('Kegiatanmodel');
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		// $status = $this->session->userdata('status');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
			$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('master_kegiatan/master_kegiatan', $data);
			$this->load->view('footer');
		}
	}

	public function save_kegiatan(){
		$session = $this->session->userdata('id');
		if (isset($_POST['submit'])) {
			$id_perusahaan = $this->input->post('id_perusahaan');
			$id_lokasi = $this->input->post('id_lokasi');
			$nama_kegiatan = $this->input->post('nama_kegiatan');
			$ket_kegiatan = $this->input->post('ket_kegiatan');
			$created = date('Y-m-d H:i:s');
			$created_by = $this->session->userdata('id');
			$modified = date('Y-m-d H:i:s');
			$modified_by = $this->session->userdata('id');

			$data = array('id_perusahaan' => $id_perusahaan,
							'id_lokasi' => $id_lokasi,
							'nama_kegiatan' => $nama_kegiatan,
							'ket_kegiatan' => $ket_kegiatan,
							'created'	=> $created,
							'created_by'	=> $created_by,
							'modified'	=> $modified,
							'modified_by'	=> $modified_by,
					);
			// var_dump($data);exit();
			$this->Kegiatanmodel->create_kegiatan($data);
			$this->session->Set_flashdata('success','success insert data');
			redirect('MasterKegiatan');
		}else{
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();			
			$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();	
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('master_kegiatan/master_kegiatan_create',$data);
			$this->load->view('footer');
		}
	}


	public function edit_kegiatan($id=null){
		$session = $this->session->userdata('id');
		if (isset($_POST['submit'])) {
			$id = $this->input->post('id');
			$id_perusahaan = $this->input->post('id_perusahaan');
			$id_lokasi = $this->input->post('id_lokasi');
			$nama_kegiatan = $this->input->post('nama_kegiatan');
			$ket_kegiatan = $this->input->post('ket_kegiatan');
			$modified = date('Y-m-d H:i:s');
			$modified_by = $this->session->userdata('id');

			$data = array('id_perusahaan' => $id_perusahaan,
							'id_lokasi' => $id_lokasi,
							'nama_kegiatan' => $nama_kegiatan,
							'ket_kegiatan' => $ket_kegiatan,
							'modified'	=> $modified,
							'modified_by'	=> $modified_by,
					);
			// var_dump($data);exit();
			$this->Kegiatanmodel->update($data,$id);
			$this->session->Set_flashdata('success','success insert data');
			redirect('MasterKegiatan');
		}else{
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();			
			$data ['KegiatanData'] = $this->Kegiatanmodel->get_id($id)->row_array();	
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('master_kegiatan/master_kegiatan_edit',$data);
			$this->load->view('footer');
		}
	}

	public function delete_kegiatan(){
		if ($this->Kegiatanmodel->delete($this->input->post('id'))) {
			return true;
		}else{
			return false;
		}
	}

	
	public function blank()
	{
			$this->load->view('header');
			$this->load->view('404');
			$this->load->view('footer');
	}

}
