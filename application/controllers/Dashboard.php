<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent:: __construct();
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$level = $this->session->userdata('level');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			// $data ['JadwalSholatData'] = $this->JadwalSholatmodel->get();
			$data ['AdminData'] = $level;
			$this->load->view('header', $data);
			$this->load->view('dashboard', $data);
			$this->load->view('footer');
		}
	}

	public function blank()
	{
			$this->load->view('header');
			$this->load->view('404');
			$this->load->view('footer');
	}

}
