<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('Usermodel');
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		// $status = $this->session->userdata('status');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			$data ['UserData'] = $this->Usermodel->get();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('user/user_list', $data);
			$this->load->view('footer');
		}
	}

	public function save(){
		$this->form_validation->set_rules('user_name', 'User Name', 'required');  
		$this->form_validation->set_rules('password', 'Password', 'required');  
		// $this->form_validation->set_rules('status', 'Status', 'required');  
		$session = $this->session->userdata('id');
		if($this->form_validation->run()=== false){
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('user/user_list', $data);
			$this->load->view('footer');
		}else{
			$user_name = $this->input->post('user_name');
			$password = $this->input->post('password');
			$status = $this->input->post('status');

			$data = array('username' => $user_name,
							'password' => $password,
							'status'	=> $status
					);
			$this->Usermodel->create($data);
			$this->session->Set_flashdata('success','success insert data');
			redirect('User');
		}
	}

	public function delete_user(){
		if ($this->Usermodel->delete($this->input->post('id'))) {
			return true;
		}else{
			return false;
		}
	}
	public function blank()
	{
			$this->load->view('header');
			$this->load->view('404');
			$this->load->view('footer');
	}

}
