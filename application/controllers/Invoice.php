<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->Model('Perusahaanmodel');
		$this->load->Model('Lokasimodel');
		$this->load->Model('Kegiatanmodel');
		$this->load->Model('Groupmodel');
		$this->load->Model('Commercialmodel');
		$this->load->Model('Invoicemodel');
	}

	public function index()
	{	
		$session = $this->session->userdata('id');
		// $status = $this->session->userdata('status');
		if (empty($session)) {
			redirect('Auth');
		}else{	
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
			$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();
			$data ['CommercialData'] = $this->Commercialmodel->get_commercial();
			$data ['InvoiceData'] = $this->Invoicemodel->get_invoice();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('invoice/invoice', $data);
			$this->load->view('footer');
		}
	}

	public function save_invoice(){
		$lastInvoice = $this->Invoicemodel->get_last_id_invoice();
		$lastNoInvoice = $lastInvoice[0]->id_invoice + 1;
		$tahun = date("m/Y");
		// var_dump($tahun);exit();
		$session = $this->session->userdata('id');
		if (isset($_POST['submit'])) {
			$id_commercial = $this->input->post('id_commercial');
			$id_perusahaan = $this->input->post('id_perusahaan');
			$kode_perusahaan = $this->Perusahaanmodel->select_nama($id_perusahaan);
			$nama_perusahaan = $this->Perusahaanmodel->select_nama($id_perusahaan);
			$id_lokasi = $this->input->post('id_lokasi');
			$nama_lokasi = $this->Lokasimodel->select_nama($id_lokasi);
			$mata_uang = $this->input->post('mata_uang');
			$tanggal_kirim = $this->input->post('tanggal_kirim');
			$tanggal_bayar = $this->input->post('tanggal_bayar');
			$term = $this->input->post('term');
			$status = 0;
			$created = date('Y-m-d H:i:s');
			$created_by = $this->session->userdata('id');
			$modified = date('Y-m-d H:i:s');
			$modified_by = $this->session->userdata('id');
			$data = array( 'id_perusahaan' => $id_perusahaan,
				'no_invoice' => $lastNoInvoice.'/'.$kode_perusahaan[0]->kode_perusahaan.'/INV/'.$tahun,
				'nama_perusahaan' => $nama_perusahaan[0]->nama_perusahaan,
				'id_lokasi' => $id_lokasi,
				'nama_lokasi' => $nama_lokasi[0]->nama_lokasi,
				'mata_uang'     => $mata_uang,		
				'tanggal_kirim' => date("Y-m-d H:i:s", strtotime($tanggal_kirim)),
				'tanggal_bayar' => date("Y-m-d H:i:s", strtotime($tanggal_bayar)),
				'term' => $term,
				'status' => $status,
				'created'	=> $created,
				'created_by'	=> $created_by,
				'modified'	=> $modified,
				'modified_by'	=> $modified_by,
			);

			$this->Invoicemodel->create_invoice($data);
			$last_id = $this->db->insert_id();
			// var_dump($id_commercial);exit();
			$data_kegiatan = $this->Invoicemodel->select_commercial_by_pdo_no($id_commercial);

			foreach ($data_kegiatan as $key => $value)
				$this->Invoicemodel->create_invoice_kegiatan([
					'id_invoice' => $last_id,
					'id_kegiatan'   => $value->id_kegiatan,
					'nama_kegiatan' => $this->Groupmodel->select_nama($value->id_kegiatan)[0]->nama_kegiatan,
					'unit'          => $value->unit,
					'harga'         => $value->harga,
					'at_cost'       => $value->at_cost,
					'created'		=> $created,
					'created_by'	=> $created_by,
					'modified'		=> $modified,
					'modified_by'	=> $modified_by,
				]);

			$this->session->Set_flashdata('success','success insert data');
			redirect('Invoice');
		}else{
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
			$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();		
			$data ['CommercialData'] = $this->Commercialmodel->get_commercial_approve();
				// $data ['CommercialKegiatan'] = $this->Commercialmodel->get_id_kegiatan();
			// print("<pre>".print_r($data ['CommercialKegiatan'], true)."</pre>");exit();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('invoice/invoice_save', $data);
			$this->load->view('footer');
		}
	}

	public function edit_invoice($id = null){
		$session = $this->session->userdata('id');
		if (isset($_POST['submit'])) {
			$id_invoice = $this->input->post('id_invoice');
			$no_invoice = $this->input->post('no_invoice');
			$id_perusahaan = $this->input->post('id_perusahaan');
			$kode_perusahaan = $this->Perusahaanmodel->select_nama($id_perusahaan);
			$nama_perusahaan = $this->Perusahaanmodel->select_nama($id_perusahaan);
			$id_lokasi = $this->input->post('id_lokasi');
			$nama_lokasi = $this->Lokasimodel->select_nama($id_lokasi);
			$mata_uang = $this->input->post('mata_uang');
			$tanggal_bayar = $this->input->post('tanggal_bayar');
			$tanggal_kirim = $this->input->post('tanggal_kirim');
			$status = $this->input->post('status');
			$created = date('Y-m-d H:i:s');
			$created_by = $this->session->userdata('id');
			$modified = date('Y-m-d H:i:s');
			$modified_by = $this->session->userdata('id');
			$data = array( 'id_perusahaan' => $id_perusahaan,
				'no_invoice' => $no_invoice,
				'nama_perusahaan' => $nama_perusahaan[0]->nama_perusahaan,
				'id_lokasi' => $id_lokasi,
				'nama_lokasi' => $nama_lokasi[0]->nama_lokasi,
				'mata_uang'     => $mata_uang,		
				'tanggal_kirim' => date("Y-m-d H:i:s", strtotime($tanggal_kirim)),
				'tanggal_bayar' => date("Y-m-d H:i:s", strtotime($tanggal_bayar)),
				'status' => $status,
				'created'	=> $created,
				'created_by'	=> $created_by,
				'modified'	=> $modified,
				'modified_by'	=> $modified_by,
			);
			$this->Invoicemodel->update_status_invoice($data, $id_invoice);
			$last_id = $this->input->post('id_invoice');
			// var_dump($last_id);exit();
			$this->Invoicemodel->delete_invoice_kegiatan($id_invoice);

			$id_invoice_kegiatan = $this->input->post('id_invoice_kegiatan');
			// $id_kegiatan = $_POST['id_kegiatan'];
			$nama_kegiatan = $_POST['nama_kegiatan'];
			$unit = $_POST['unit'];
			$harga = $_POST['harga'];
			$at_cost = $_POST['at_cost'];
			// var_dump($nama_kegiatan);exit();

			$actual = [];
			for ($i = 0; $i < count($nama_kegiatan); $i++)
			$actual[] = [
				'id_invoice' 	=> $last_id,
				// 'id_kegiatan'   => $id_kegiatan[$i],
				'nama_kegiatan' => $nama_kegiatan[$i],//$this->Groupmodel->select_nama($id_kegiatan[$i])[0]->nama_kegiatan,
				'unit'          => $unit[$i],
				'harga'         => $harga[$i],
				'at_cost'       => $at_cost[$i],
				'modified'		=> $modified,
				'modified_by'	=> $modified_by,
			];
			// var_dump($actual);exit();
			foreach ($actual as $key => $value) {
				$this->Invoicemodel->create_invoice_kegiatan($value);
			}
		// print('<pre>'.print_r($data2 , true).'</pre>');exit();
		// $this->Commercialmodel->create_commercial_kegiatan($new_data);
			$this->session->Set_flashdata('success','success insert data');
			redirect('Invoice');
			}else{
				$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
				$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
				$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();		
				$data ['InvoiceData'] = $this->Invoicemodel->get_id_invoice($id)->row_array();
				$data ['InvoiceKegiatan'] = $this->Invoicemodel->get_id_invoice_kegiatan($id);
			// print("<pre>".print_r($data ['CommercialKegiatan'], true)."</pre>");exit();
				$data ['AdminData'] = $session;
				$this->load->view('header', $data);
				$this->load->view('invoice/invoice_edit', $data);
				$this->load->view('footer');
			}
		}

		public function approve_invoice($id = null){
			$session = $this->session->userdata('id');
			if (isset($_POST['approve'])) {
				$id = $this->input->post('id_invoice');
				$status 	   = '1';
				$modified = date('Y-m-d H:i:s');
				$modified_by = $this->session->userdata('id');
				$data = array(
					'modified' 		=> $modified,
					'modified_by' 	=> $modified_by,
					'status' 		=> $status,
				);
				// var_dump($data);exit();
				$this->Invoicemodel->update_status_invoice($data, $id);
				$this->session->Set_flashdata('success','success insert data');
				redirect('Invoice');

			}elseif (isset($_POST['cencel'])) {
				$id = $this->input->post('id_invoice');
				$status 	   = '2';
				$modified = date('Y-m-d H:i:s');
				$modified_by = $this->session->userdata('id');
				$data = array(
					'modified' 		=> $modified,
					'modified_by' 	=> $modified_by,
					'status' 		=> $status,
				);

				$this->Invoicemodel->update_status_invoice($data, $id);
				$this->session->Set_flashdata('success','success insert data');
				redirect('Invoice');
			}elseif (isset($_POST['approve_cancel'])) {
				$id = $this->input->post('id_invoice');
				$status 	   = '0';
				$keterangan    = $this->input->post('keterangan');
				$modified = date('Y-m-d H:i:s');
				$modified_by = $this->session->userdata('id');
				$data = array(
					'modified' 		=> $modified,
					'modified_by' 	=> $modified_by,
					'status' 		=> $status,
					'keterangan' 		=> $keterangan,
				);
				// var_dump($data);exit();
				$this->Invoicemodel->update_status_invoice($data, $id);
				$this->session->Set_flashdata('success','success insert data');
				redirect('Invoice');
			}else{
				$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
				$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
				$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();		
				$data ['InvoiceData'] = $this->Invoicemodel->get_id_invoice($id)->row_array();
				$data ['InvoiceKegiatan'] = $this->Invoicemodel->get_id_invoice_kegiatan($id);
			// print("<pre>".print_r($data ['InvoiceKegiatan'], true)."</pre>");exit();
				$data ['AdminData'] = $session;
				$this->load->view('header', $data);
				$this->load->view('invoice/invoice_approve', $data);
				$this->load->view('footer');
			}
		}


		public function detail_invoice($id = null){
			$session = $this->session->userdata('id');
			$data ['PerusahaanData'] = $this->Perusahaanmodel->get_perusahaan();
			$data ['LokasiData'] = $this->Lokasimodel->get_lokasi();
			$data ['KegiatanData'] = $this->Kegiatanmodel->get_kegiatan();		
			$data ['InvoiceData'] = $this->Invoicemodel->get_id_invoice($id)->row_array();
			$data ['InvoiceKegiatan'] = $this->Invoicemodel->get_id_invoice_kegiatan($id);
			// print("<pre>".print_r($data ['InvoiceData'], true)."</pre>");exit();
			$data ['AdminData'] = $session;
			$this->load->view('header', $data);
			$this->load->view('invoice/invoice_detail', $data);
			$this->load->view('footer');
		}

		public function report_pdf_detail_invoice($id=null){
			$data = array(
				'PerusahaanData' => $this->Perusahaanmodel->get_perusahaan(),
				'LokasiData' => $this->Lokasimodel->get_lokasi(),
				'KegiatanData' => $this->Kegiatanmodel->get_kegiatan(),		
				'InvoiceData' => $this->Invoicemodel->get_id_invoice($id)->row_array(),
				'InvoiceKegiatan' => $this->Invoicemodel->get_id_invoice_kegiatan($id),
			);
			// var_dump(json_encode($data['InvoiceData']));exit();
			$this->load->view('laporan_pdf_detail_invoice', $data);
		}

		public function cari_pda_no(){
			$pda_no = $this->input->post('pda_no');
			$data = $this->Invoicemodel->get_pda_no($pda_no);
			echo json_encode($data);
		}

		public function delete_invoice(){
			if ($this->Invoicemodel->delete_invoice($this->input->post('id'))) {
				return true;
			}else{
				return false;
			}
		}
	}