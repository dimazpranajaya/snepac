<?php defined('BASEPATH') OR exit('No direct script access allowed');

define('DOMPDF_ENABLE_AUTOLOAD', false);
require_once("./vendor/dompdf/dompdf/dompdf_config.inc.php");

class PdfController extends CI_Controller {
	public function index(){
		$html=$this->load->view("kp",'', true); 
		$filename='';
		$stream=TRUE;
		$paper = 'A4'; 
		$orientation = "portrait";
		// $orientation = "landscape";

		$dompdf = new DOMPDF();
	    $dompdf->load_html($html);
	    $dompdf->set_paper($paper, $orientation);
	    $dompdf->render();
	    if ($stream) {
	        $dompdf->stream($filename.".pdf", array("Attachment" => 0));
	    } else {
	        return $dompdf->output();
	    }
	}
} 