 <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-info alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success!</strong> Item created successfully.
    </div>      
<?php } ?>
<?php if ($this->session->flashdata('update')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success!</strong> Item update successfully.
    </div>      
<?php } ?>
 <div class="card shadow mb-4">
  <div class="card-header py-3">
    <a href="#" style="float: right;" class="btn btn-primary btn-icon-split btn-sm" data-toggle="modal" data-target="#add">
      <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
      </span>
      <span class="text">Add</span>
    </a>
    <h6 class="m-0 font-weight-bold text-primary">List User</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>User Name</th>
            <th>Status</th>
            <th width="45" style="text-align: center;">Set</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>User Name</th>
            <th>Status</th>
            <th style="text-align: center;">Set</th>
          </tr>
        </tfoot>
        <tbody>
          <?php 
          $no=1;
          foreach($UserData as $key =>$value){?>
          <tr>
            <td><?php echo $no++?></td>
            <td><?php echo $value->username?></td>
            <td><?php if ($value->status == '1' ) echo 'Aktif'; else echo 'Non Aktif'; ?></td>
            <td style="text-align: center;">
                <a href="" class="btn btn-xs btn-primary btn-circle btn-sm" data-toggle="modal" data-target="#addModal"><i class="fa fa-edit"></i></a>
                <a href="" class="btn btn-xs btn-danger btn-circle btn-sm" onclick="delete_data(<?php echo $value->id?>)"><i class="fas fa-trash"></i></a>              
            </td>
          </tr>         
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</div>

  <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="col-12">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Form Registrasi User.</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form action="<?php echo base_url();?>User/save" method="post">
        <div class="container">
              <div class="form-group">
                <label for="user_name">User Name:</label>
                <input type="user_name" class="form-control" id="user_name" placeholder="Enter User Name" name="user_name" required>
              </div>
              <div class="form-group">
                <label for="password">Password :</label>
                <input type="password" class="form-control" id="password" placeholder="Enter password" name="password" required>
              </div>
              <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" id="status" name="status" >
                  <option value="1">Aktif</option>
                  <option value="0">Non Aktif</option>
                </select>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
    </div>
  </div>

  <script type="text/javascript">
    function delete_data(id)
      {
        swal({
          title: "Are you sure?",
          text: "Your will not be able to recover this file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(){
          $.ajax({
            url   : "<?php echo base_url('User/delete_user/')?>",
            type  : "post",
            data  : {id:id},
            success : function(){
              swal("Deleted!", "Your file has been deleted.", "success");
              window.location.reload();
            },
            error : function(){
              swal("gagal", "cdvsdvdsvdvf","error");
            }
          });
        });
      }
  </script>