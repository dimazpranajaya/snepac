<?php
  $contenct =
  "<style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 50%;
    }

    td, th {
      /*border: 1px solid #dddddd;*/
      text-align: left;
      padding: 3px;
    }

    tr:nth-child(even) {
      background-color: #dddddd;
    }
  </style>";

  $contenct .=
  "<page>
      <div>
      <br>
      <br>
      <img width='755' src='./assets/img/header.jpg'>
      <br>
      <br>
        <br>
      <h3 style='text-align:center'>INVOICE</h3>
      <br>
      <br>
      <table>
        <tr>
            <td style='width: 10px;'></td>
            <td style='width: 90px;'>Pay Date</td>
            <td style='width: ;'>:</td>
            <td style='width: 270px;'>".date('d F Y', strtotime($InvoiceData['tanggal_bayar']))."</td>
            <td style='width: 90px;'>Vessel</td>
            <td style='width: ;'>:</td>
            <td style='width: 240px;'>".$InvoiceData['nama_kapal']."</td>
         </tr>
         <tr>
            <td style='width: 10px;'></td>
            <td style='width: 90px;'>To</td>
            <td style='width: ;'>:</td>
            <td style='width: 270px;'>".$InvoiceData['nama_perusahaan']."</td>
            <td style='width: 90px;'>GRT</td>
            <td style='width: ;'>:</td>
            <td style='width: 240px;'>".$InvoiceData['grt']."</td>
         </tr>
         <tr>
            <td style='width: 10px;'></td>
            <td style='width: 90px;'>Attn</td>
            <td style='width: ;'>:</td>
            <td style='width: 270px;'>".$InvoiceData['nama_owner']."</td>
            <td style='width: 90px;'>Port</td>
            <td style='width: ;'>:</td>
            <td style='width: 240px;'>".$InvoiceData['nama_lokasi']."</td>
         </tr>
         <tr>
            <td style='width: 10px;'></td>
            <td style='width: 90px;'>PDA No</td>
            <td style='width: ;'>:</td>
            <td style='width: 270px;'>".$InvoiceData['no_invoice']."</td>
            <td style='width: 90px;'>Sent Date</td>
            <td style='width: ;'>:</td>
            <td style='width: 240px;'>".date('d F Y', strtotime($InvoiceData['tanggal_kirim']))."</td>
         </tr>
      </table>
      <br>
      <br>
      <table border='1'>
      <tr>
            <th style='width: 10px;text-align: center;border:none;border-right:1px solid black'></th>
            <th style='width: 20px;text-align: center;'>NO</th>
            <th style='width: 280px;'>ACTIVITY</th>
            <th style='width: 50px;text-align: center;'>UNIT</th>
            <th style='width: 90px;text-align: center;'>UNIT PRICE</th>
            <th style='width: 90px;text-align: center;'>AT COST</th>
            <th style='width: 90px;text-align: center;'>AMOUNT</th>
          </tr>"; 
    $t_harga = 0;
    $t_at_cost = 0;
    $t_amount = 0;
    foreach ($InvoiceKegiatan as $value) {
      $contenct .= "<tr>
            <td style='text-align: center;border:none;border-right:1px solid black'></td>
            <td style='text-align: center;'>1.</td>
            <td>".$value->nama_kegiatan."</td>
            <td style='text-align: center;'>".$value->unit."</td>
            <td style='text-align: center;'>".$value->harga."</td>
            <td style='text-align: center;'>".$value->at_cost."</td>
            <td style='text-align: center;'>".(($value->unit*$value->harga)+$value->at_cost)."</td>
          </tr>";
      $t_harga = $t_harga + $value->harga;
      $t_at_cost = $t_at_cost + $value->at_cost;
      $t_amount = $t_amount + (($value->unit*$value->harga)+$value->at_cost);
    }
    $contenct.="
    <tr>
            <td style='text-align: center;border:none;border-right:1px solid black'></td>
            <td style='text-align: center;' colspan='3'> TOTAL ACTIVITY</td>
            <td style='text-align: center;'>".$t_harga."</td>
            <td style='text-align: center;'>".$t_at_cost."</td>
            <td style='text-align: center;'>".$t_amount."</td>
          </tr>
    </table>";
  
  $contenct.="
      </div>
      <br>
      <br>
     

    </page>";
// var_dump($contenct);exit();
  require_once('assets/html2pdf/html2pdf.class.php');
  $html2pdf = new html2pdf('P', 'A4', 'en');
  $html2pdf->writeHTML($contenct);
  $html2pdf->Output('report_pdf_detail.pdf');
 ?>