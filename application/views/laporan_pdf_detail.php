<?php
	$contenct =
	"<style>
		table {
		  font-family: arial, sans-serif;
		  border-collapse: collapse;
		  width: 50%;
		}

		td, th {
		  /*border: 1px solid #dddddd;*/
		  text-align: left;
		  padding: 3px;
		}

		tr:nth-child(even) {
		  background-color: #dddddd;
		}
	</style>";

	$contenct .=
	"<page>
			<div>
			<br>
			<br>
			<img width='755' src='./assets/img/header.jpg'>
			<br>
			<br>
				<br>
			<h3 style='text-align:center'>COMMERCIAL</h3>
			<br>
			<br>
			<table>
				<tr>
				    <td style='width: 10px;'></td>
				    <td style='width: 60px;'>Date</td>
				    <td style='width: ;'>:</td>
				    <td style='width: 270px;'>".date('d F Y', strtotime($CommercialData['tanggal']))."</td>
				    <td style='width: 60px;'>Vessel</td>
				    <td style='width: ;'>:</td>
				    <td style='width: 240px;'>".$CommercialData['nama_kapal']."</td>
				 </tr>
				 <tr>
				    <td style='width: 10px;'></td>
				    <td style='width: 60px;'>To</td>
				    <td style='width: ;'>:</td>
				    <td style='width: 270px;'>".$CommercialData['nama_perusahaan']."</td>
				    <td style='width: 60px;'>GRT</td>
				    <td style='width: ;'>:</td>
				    <td style='width: 240px;'>".$CommercialData['grt']."</td>
				 </tr>
				 <tr>
				    <td style='width: 10px;'></td>
				    <td style='width: 60px;'>Attn</td>
				    <td style='width: ;'>:</td>
				    <td style='width: 270px;'>".$CommercialData['nama_owner']."</td>
				    <td style='width: 60px;'>Port</td>
				    <td style='width: ;'>:</td>
				    <td style='width: 240px;'>".$CommercialData['nama_lokasi']."</td>
				 </tr>
				 <tr>
				    <td style='width: 10px;'></td>
				    <td style='width: 60px;'>PDA No</td>
				    <td style='width: ;'>:</td>
				    <td style='width: 270px;'>".$CommercialData['pda_no']."</td>
				    <td style='width: 60px;'></td>
				    <td style='width: ;'></td>
				    <td style='width: 240px;'></td>
				 </tr>
			</table>
			<br>
			<br>
			<table border='1'>
			<tr>
				    <th style='width: 10px;text-align: center;border:none;border-right:1px solid black'></th>
				    <th style='width: 20px;text-align: center;'>NO</th>
				    <th style='width: 280px;'>ACTIVITY</th>
				    <th style='width: 50px;text-align: center;'>UNIT</th>
				    <th style='width: 90px;text-align: center;'>UNIT PRICE</th>
				    <th style='width: 90px;text-align: center;'>AT COST</th>
				    <th style='width: 90px;text-align: center;'>AMOUNT</th>
				  </tr>"; 
		$t_harga = 0;
		$t_at_cost = 0;
		$t_amount = 0;
		foreach ($CommercialKegiatan as $value) {
			$contenct .= "<tr>
				    <td style='text-align: center;border:none;border-right:1px solid black'></td>
				    <td style='text-align: center;'>1.</td>
				    <td>".$value->nama_kegiatan."</td>
				    <td style='text-align: center;'>".$value->unit."</td>
				    <td style='text-align: center;'>".number_format($value->harga_dasar, 2,',','.')."</td>
				    <td style='text-align: center;'>".number_format($value->at_cost, 2,',','.')."</td>
				    <td style='text-align: center;'>".number_format((($value->unit*$value->harga)+$value->at_cost), 2,',','.')."</td>
				  </tr>";
			// $t_harga += $value->harga_dasar * $value->unit;
			$t_harga = $t_harga + ($value->harga_dasar * $value->unit);
			$t_at_cost = $t_at_cost + $value->at_cost;
			$t_amount = $t_amount + (($value->unit*$value->harga_dasar)+$value->at_cost);
		}
		$contenct.="
		<tr>
				    <td style='text-align: center;border:none;border-right:1px solid black'></td>
				    <td style='text-align: center;' colspan='3'> TOTAL ACTIVITY</td>
				    <td style='text-align: center;'>".number_format($t_harga, 2,',','.')."</td>
				    <td style='text-align: center;'>".number_format($t_at_cost, 2,',','.')."</td>
				    <td style='text-align: center;'>".number_format($t_amount, 2,',','.')."</td>
				  </tr>
		</table>";
	
	$contenct.="
			</div>
			<br>
			<br>
			<table>
			<tr>
			    <td>&nbsp;&nbsp;&nbsp;</td>
			    <td colspan='2'>1. This quotation valid in batam based on this activity only</td>
			  </tr>
			  <tr>
			    <td>&nbsp;&nbsp;&nbsp;</td>
			    <td colspan='2'>2. Any activity excluded in Quetation subjects to approval prior to execution</td>
			  </tr>
			   <tr>
			    <td>&nbsp;&nbsp;&nbsp;</td>
			    <td colspan='2'>3. PDA should be settled before vessel arrival, final invoice will be settled after vessel departure</td>
			  </tr>
			</table>
			<br>
			<br>
			<table>
			  <tr>
			    <td>&nbsp;&nbsp;&nbsp;</td>
			    <td>Benefiticiary Bank Name</td>
			    <td>&nbsp;&nbsp;&nbsp;&nbsp;: Bank Mandiri</td>
			  </tr>
			  <tr>
			    <td>&nbsp;&nbsp;&nbsp;</td>
			    <td>Branch</td>
			    <td>&nbsp;&nbsp;&nbsp;&nbsp;: Cab. Batuampar</td>
			  </tr>
			  <tr>
			    <td>&nbsp;&nbsp;&nbsp;</td>
			    <td>Beneficiary Bank Address</td>
			    <td>&nbsp;&nbsp;&nbsp;&nbsp;: JL. Yos Sudarso - Sei Jodoh, Batuampar Batam</td>
			  </tr>
			</table>

		</page>";
// var_dump($contenct);exit();
	require_once('assets/html2pdf/html2pdf.class.php');
	$html2pdf = new html2pdf('P', 'A4', 'en');
	$html2pdf->writeHTML($contenct);
	$html2pdf->Output('report_pdf_detail.pdf');
 ?>