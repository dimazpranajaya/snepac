 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />   -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Perusahaan</h6>
      </div>
      <div class="card-body">
        <form action="" method="post">
          <input type="hidden" name="id_perusahaan" value="<?php echo $PerusahaanData['id_perusahaan']?>">
          <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-3 col-form-label">Kode </label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $PerusahaanData['kode_perusahaan']?>">
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-3 col-form-label">Perusahaan </label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $PerusahaanData['nama_perusahaan']?>">
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-3 col-form-label">Owner</label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $PerusahaanData['nama_owner']?>">
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-3 col-form-label">Kode Perushaan</label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $PerusahaanData['kode_perusahaan']?>">
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-3 col-form-label">Alamat </label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $PerusahaanData['alamat']?>">
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-3 col-form-label">Keterangan </label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $PerusahaanData['ket_perusahaan']?>">
		    </div>
		  </div>
		  <br>
		 <table class="table table-bordered">
		 	<tr>
		 		<th>No</th>
		 		<th>Kapal</th>
		 		<th>IMO</th>
		 		<th>GRT</th>
		 		<th>Ket</th>
		 	</tr>
		 	<?php 
		 	$no = 1;
		 	foreach ($KapalData as $value){?>	 		
		 	<tr>
		 		<td><?php echo $no++ ?>.</td>
		 		<td><?php echo $value->nama_kapal?></td>
		 		<td><?php echo $value->imo?></td>
		 		<td><?php echo $value->grt?></td>
		 		<td><?php echo $value->keterangan?></td>
		 		<!-- <td><?php echo number_format($value->harga_dasar, 2,',','.');?></td> -->
		 		<!-- <td><?php echo number_format($value->at_cost, 2,',','.');?></td> -->
		 	</tr>
		 <?php }?>
		 	<!-- <tr>
		 		<th colspan="3" style="text-align: center;">Total</th>
		 		<th>Harga</th>
		 		<th>At Cost (%)</th>
		 	</tr> -->
		 </table>
			<br>
        <div class="modal-footer">
          <a class="btn btn-secondary" href="<?php echo base_url();?>MasterPerusahaan" style="width: 90px;">Kembali</a>
          <!-- <button name="approve" class="btn btn-success" onclick="approve_data(<?php echo $PerusahaanData['id_invoice']?>)" style="width: 90px;color:white" ></button> -->
          <!-- <a href="<?php echo base_url();?>Master_perusahaan/" class="btn btn-danger" target="_blank" style="width: 90px;color:white"><i class="fa fa-file-pdf"></i>  PDF</a> -->
        </div>
        </form>



      </div>
    </div>
  </div>
</div>

   
<script type="text/javascript">
    function approve_data(id){
          $.ajax({
            url   : "<?php echo base_url('Commercial/detail_commercial/')?>",
            type  : "post",
            data  : {id:id},
            success : function(data){
               swal("Success!", "Berhasil Di Setujui!", "success");
               // window.location.reload();
            },
            error : function(data){
              swal("Error", "Your file not deleted","error");
            }
          });
    }

     function cancel_data(id){
          $.ajax({
            url   : "<?php echo base_url('Commercial/detail_commercial/')?>",
            type  : "post",
            data  : {id:id},
            success : function(data){
               swal("Success!", "Batalkan!", "error");
               // window.location.reload();
            },
            error : function(data){
              swal("Error", "Your file not deleted","error");
            }
          });
    }

</script>