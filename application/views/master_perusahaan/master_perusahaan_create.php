<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Perusahaan</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>MasterPerusahaan/save_perusahaan" method="post">
          <div class="form-group row">
            <div class="col-lg-3">
              <label for="kode_perusahaan">Kode :</label>
              <input type="text" class="form-control" id="kode_perusahaan" name="kode_perusahaan" placeholder="Kode Perusahaan" required>
            </div>
            <div class="col-lg-5">
              <label for="perusahaan">Perusahaan :</label>
              <input type="text" class="form-control" id="perusahaan" name="perusahaan" placeholder="Perusahaan" required>
            </div>
            <div class="col-lg-4">
              <input type="checkbox" id="show" name="show" value="">
              <label for="vehicle1"> Pemilik (Owner) :</label><br>
              <input type="text" class="form-control" id="nama_owner" name="nama_owner" placeholder="Owner" disabled="">
            </div>

          </div>
          <!-- <div class="form-group row"> -->

           <!-- <div class="col-lg-6 "> -->
              <!-- <label for="nama_kapal">Kapal :</label>
                <input type="text" class="form-control" id="nama_kapal" name="nama_kapal" placeholder="Kapal" required> -->
                
                    
                    <div class="form-group row">
                      <div class="col-lg-6 ">
                        <label for="alamat">Alamat :</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-lg-6 ">
                        <label for="ket_perusahaan">Keterangan :</label>
                        <textarea type="text" class="form-control" rows="2" id="ket_perusahaan" name="ket_perusahaan" placeholder="Keterangan" required></textarea>
                      </div>
                    </div>
                    <table class="" id="dynamic">  
                      <tr>
                        <td>Kapal :</td>
                        <td>IMO :</td>
                        <td>GRT :</td>
                        <td>Keterangan :</td>
                      </tr>
                      <tr>  
                          <td width="300"><input type="text" name="nama_kapal[]" placeholder="Nama Kapal" class="form-control name_list" id="nama_kapal" disabled/></td> 
                          <td width="200"><input type="number" name="imo[]" placeholder="Imo" class="form-control name_list" id="imo" disabled/></td>
                          <td width="200"><input type="number" name="grt[]" placeholder="Grt" class="form-control name_list" id="grt" disabled/></td>
                          <td width="300"><input type="text" name="ket[]" placeholder="Keterangan" class="form-control name_list" id="ket" disabled/></td>   
                          <td><button type="button" name="add" id="add" class="btn btn-success" id="" disabled><i class="fas fa-plus"></i></button></td>  
                      </tr>  
                    </table>
                    <br>


                    <div class="form-group row" style="float: right;">
                      <!-- <div class="modal-footer"> -->
                        <div class="col-lg-12">
                          <a class="btn btn-secondary" href="<?php echo base_url();?>MasterPerusahaan">Close</a>
                          <button type="submit" name="submit" class="btn btn-primary">Save</button>
                        </div>
                        <!-- </div> -->
                    </div>
                      <br>
                    
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <script type="text/javascript">
              $(document).ready(function(){      
                var i=1;  
                $('#add').click(function(){  
                 i++;  
                 $('#dynamic').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="nama_kapal[]" placeholder="Nama Kapal" class="form-control name_list" required /></td><td><input type="number" name="imo[]" placeholder="Imo" class="form-control name_list" required /></td><td><input type="number" name="grt[]" placeholder="Grt" class="form-control name_list" required /></td><td><input type="text" name="ket[]" placeholder="Keterangan" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class="fas fa-minus"></i></button></td></tr>');  
               });

                $(document).on('click', '.btn_remove', function(){  
                 var button_id = $(this).attr("id");   
                 $('#row'+button_id+'').remove();  
               });  

              });  

              $('#show').click(function(){
                var cek = $(this).prop('checked');
                if (cek) {
                  $('#nama_owner').prop('disabled', false);
                  $('#nama_kapal').prop('disabled', false);
                  $('#imo').prop('disabled', false);
                  $('#grt').prop('disabled', false);
                  $('#ket').prop('disabled', false);
                  $('#add').prop('disabled', false);
                }else{
                  $('#nama_owner').prop('disabled', true);
                  $('#nama_kapal').prop('disabled', true);
                  $('#imo').prop('disabled', true);
                  $('#grt').prop('disabled', true);
                  $('#ket').prop('disabled', true);
                  $('#add').prop('disabled', true);
                }
              });
            </script>