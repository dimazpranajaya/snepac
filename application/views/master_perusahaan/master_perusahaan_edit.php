 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />   -->
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 <div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Perusahaan</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>MasterPerusahaan/edit_perusahaan" method="post">
          <input type="hidden" name="id_perusahaan" name="id_perusahaan" value="<?php echo $PerusahaanData['id_perusahaan']?>">
          <div class="form-group row">
            <div class="col-lg-3">
              <label for="kode_perusahaan">Kode :</label>
              <input type="text" class="form-control" id="kode_perusahaan" name="kode_perusahaan" value="<?php echo $PerusahaanData['kode_perusahaan']?>" required>
            </div>
            <div class="col-lg-5">
              <label for="perusahaan">Perusahaan :</label>
              <input type="text" class="form-control" id="perusahaan" name="perusahaan" value="<?php echo $PerusahaanData['nama_perusahaan']?>" required>
            </div>
            <div class="col-lg-4">
              <!-- <input type="checkbox" id="show" name="show" value=""> -->
              <label for="vehicle1"> Pemilik (Owner) :</label><br>
              <input type="text" class="form-control" id="nama_owner" name="nama_owner" placeholder="Owner">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-lg-6 ">
              <label for="alamat">Alamat :</label>
              <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $PerusahaanData['alamat']?>" required>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-lg-6 ">
              <label for="ket_perusahaan">Keterangan :</label>
              <textarea type="text" class="form-control" rows="2" id="ket_perusahaan" name="ket_perusahaan" required><?php echo $PerusahaanData['ket_perusahaan']?></textarea>
            </div>
          </div>
          <table class="" id="dynamic_field">  
            <tr>
              <td>Kapal :</td>
              <td>IMO :</td>
              <td>GRT :</td>
              <td>Keterangan :</td>
            </tr>
            <?php foreach ($KapalData as $row => $value1) { ?>  
              <tr>  
                <td width="300"><input type="text" name="nama_kapal[]" placeholder="Nama Kapal" class="form-control name_list" value="<?php echo $value1->nama_kapal?>" required="" /></td> 
                <td width="200"><input type="number" name="imo[]" placeholder="Imo" class="form-control name_list" value="<?php echo $value1->imo?>" required="" /></td>
                <td width="200"><input type="number" name="grt[]" placeholder="Grt" class="form-control name_list" value="<?php echo $value1->grt?>" required="" /></td>
                <td width="300"><input type="text" name="ket[]" placeholder="Keterangan" class="form-control name_list" value="<?php echo $value1->keterangan?>" required="" /></td>   
                <?php if ($row === 0) echo '<td><button type="button" name="add" id="add" class="btn btn-success"><i class="fas fa-plus"></i></button></td>';
                else echo '<td><button type="button" name="remove" class="btn btn-danger btn_remove"><i class="fas fa-minus"></i></button></td>';?>
              </tr> 
            <?php }?> 
          </table> 
          <br>

          <div class="form-group row" style="float: right;">
            <!-- <div class="modal-footer"> -->
              <div class="col-lg-12">
                <a class="btn btn-secondary" href="<?php echo base_url();?>MasterPerusahaan">Close</a>
                <button type="submit" name="submit" class="btn btn-primary">Save</button>
              </div>
              <!-- </div> -->
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){      
      var i=1;  
      $('#add').click(function(){  
       i++;  
       $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="nama_kapal[]" placeholder="Nama Kapal" class="form-control name_list" required /></td><td><input type="number" name="imo[]" placeholder="Imo" class="form-control name_list" required /></td><td><input type="number" name="grt[]" placeholder="Grt" class="form-control name_list" required /></td><td><input type="text" name="ket[]" placeholder="Keterangan" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class="fas fa-minus"></i></button></td></tr>');  
     });

      $(document).on('click', '.btn_remove', function(){  
       var button_id = $(this).attr("id");   
       $('#row'+button_id+'').remove();  
     });  

      $("#dynamic_field").on('click','.btn_remove',function(){
        $(this).parent().parent().remove();
      });
    });  

     $('#show').click(function(){
        var cek = $(this).prop('checked');
        if (cek) {
          $('#nama_owner').prop('disabled', false);
          $('#nama_kapal').prop('disabled', false);
          $('#imo').prop('disabled', false);
          $('#grt').prop('disabled', false);
          $('#ket').prop('disabled', false);
          $('#add').prop('disabled', false);
        }else{
          $('#nama_owner').prop('disabled', true);
          $('#nama_kapal').prop('disabled', true);
          $('#imo').prop('disabled', true);
          $('#grt').prop('disabled', true);
          $('#ket').prop('disabled', true);
          $('#add').prop('disabled', true);
        }
      });
  </script>