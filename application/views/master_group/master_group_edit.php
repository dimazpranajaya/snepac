<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Edit</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>MasterGroup/edit_group" method="post">
        <input type="hidden" id="id_group" name="id_group" value="<?php echo $GroupData['id_group']?>">  
        <div class="container">
              <div class="form-group">
                <label for="nama_group">Nama Group :</label>
                <input type="text" class="form-control" id="nama_group" placeholder="Nama Group" name="nama_group" value="<?php echo $GroupData['nama_group']?>" required>  
              </div>
              <div class="form-group">
                <label for="keterangan">Keterangan :</label>
                <textarea type="text" class="form-control" rows="3" id="keterangan" name="keterangan" placeholder="Keterangan" required><?php echo $GroupData['keterangan']?></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <a class="btn btn-secondary" href="<?php echo base_url();?>MasterGroup">Close</a>
            <button type="submit" name="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
