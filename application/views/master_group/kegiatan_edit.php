<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Edit</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>MasterGroup/edit_kegiatan" method="post">
        <input type="hidden" id="id_kegiatan" name="id_kegiatan" value="<?php echo $GroupDataList['id_kegiatan']?>">  
        <div class="container">
              <input type="hidden" name="id_group" class="form-control" name="id_group" value="<?php echo $GroupDataList['id_group']?>">
              
              <div class="form-group row">
                <label for="nama_kegiatan">Kegiatan :</label>
                <textarea type="text" class="form-control" rows="2" id="nama_kegiatan" name="nama_kegiatan" placeholder="Nama Kegiatan"><?php echo $GroupDataList['nama_kegiatan']?></textarea>
              </div>
              <div class="form-group row">
                <label for="keterangan">Keterangan :</label>
                <textarea type="text" class="form-control" rows="2" id="ket_kegiatan" name="ket_kegiatan" placeholder="Keterangan"><?php echo $GroupDataList['ket_kegiatan']?></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <a class="btn btn-secondary" href="<?php echo base_url();?>MasterGroup/list_kegiatan/<?php echo $GroupDataList['id_group']?>">Close</a>
            <button type="submit" name="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
