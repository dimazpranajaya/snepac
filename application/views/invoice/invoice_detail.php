 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />   -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Invoice Approval Form</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>Invoice/detail_invoice" method="post">
          <input type="hidden" name="id_invoice" value="<?php echo $InvoiceData['id_invoice']?>">
          <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-1"></label>
		    <label class="col-sm-3 col-form-label">Perusahaan </label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $InvoiceData['nama_perusahaan']?>">
		    </div>
		    <div class="col-sm-2">
		    	<?php if ($InvoiceData['status'] == 1){
		    		echo "<a class='m-0 font-weight-bold text-success' style='font-size:20px;text-decoration: underline;'>APPROVED</a>";
		    	}else{
		    		echo "<a class='m-0 font-weight-bold text-danger' style='font-size:20px;text-decoration: underline;'>CANCEL</a>";
		    	}?>
		      <!-- <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $InvoiceData['status']?>"> -->
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-1"></label>
		    <label class="col-sm-3 col-form-label">Port </label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $InvoiceData['nama_lokasi']?>">
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-1"></label>
		    <label class="col-sm-3 col-form-label">Kegiatan </label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $InvoiceData['nama_kegiatan']?>">
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-1"></label>
		    <label class="col-sm-3 col-form-label">Mata Uang </label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $InvoiceData['mata_uang']?>">
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
		    <label class="col-sm-1"></label>
		    <label class="col-sm-3 col-form-label">Harga </label>
		    <label class="col-form-label">:</label>
		    <div class="col-sm-5">
		      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $InvoiceData['total_harga']?>">
		    </div>
		  </div>
		  <div class="form-group row" style="margin-bottom: -10px;">
	        <label class="col-sm-1"></label>
	        <label class="col-sm-3 col-form-label">Tanggal</label>
	        <label class="col-form-label"></label>
	        <div class="col-sm-5">
	        </div>
	      </div>
			  <div class="form-group row" style="margin-bottom: -10px;">
			    <label class="col-sm-2"></label>
			    <label class="col-sm-2">Kirim</label>
			    <label class="col-form-label">:</label>
			    <div class="col-sm-5">
			      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo date('d F Y', strtotime($InvoiceData['tanggal_kirim']))?>">
			    </div>
			  </div>
	      <div class="form-group row">
	        <label class="col-sm-2"></label>
	        <label class="col-sm-2">Bayar</label>
	        <label class="col-form-label">:</label>
	        <div class="col-sm-5">
	          <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo date('d F Y', strtotime($InvoiceData['tanggal_bayar']))?>">
	        </div>
	      </div>
		 <table class="table table-bordered">
		 	<tr>
		 		<th>No</th>
		 		<th>Kegiatan</th>
		 		<th>Unit</th>
		 		<th>Harga</th>
		 		<th>At Cost (%)</th>
		 	</tr>
		 	<?php 
		 	$no = 1;
		 	foreach ($InvoiceKegiatan as $value){?>	 		
		 	<tr>
		 		<td><?php echo $no++ ?>.</td>
		 		<td><?php echo $value->nama_kegiatan?></td>
		 		<td><?php echo $value->unit?></td>
		 		<td><?php echo number_format($value->harga, 2,',','.');?></td>
		 		<td><?php echo number_format($value->at_cost, 2,',','.');?></td>
		 	</tr>
		 <?php }?>
		 	<!-- <tr>
		 		<th colspan="3" style="text-align: center;">Total</th>
		 		<th>Harga</th>
		 		<th>At Cost (%)</th>
		 	</tr> -->
		 </table>
			<br>
        <div class="modal-footer">
          <a class="btn btn-secondary" href="<?php echo base_url();?>Invoice" style="width: 90px;">Kembali</a>
          <!-- <button name="approve" class="btn btn-success" onclick="approve_data(<?php echo $InvoiceData['id_invoice']?>)" style="width: 90px;color:white" ></button> -->
          <a href="<?php echo base_url();?>Invoice/report_pdf_detail_invoice/<?php echo $InvoiceData['id_invoice']?>" class="btn btn-danger" target="_blank" style="width: 90px;color:white"><i class="fa fa-file-pdf"></i>  PDF</a>
        </div>
        </form>



      </div>
    </div>
  </div>
</div>

   
<script type="text/javascript">
    function approve_data(id){
          $.ajax({
            url   : "<?php echo base_url('Commercial/detail_commercial/')?>",
            type  : "post",
            data  : {id:id},
            success : function(data){
               swal("Success!", "Berhasil Di Setujui!", "success");
               // window.location.reload();
            },
            error : function(data){
              swal("Error", "Your file not deleted","error");
            }
          });
    }

     function cancel_data(id){
          $.ajax({
            url   : "<?php echo base_url('Commercial/detail_commercial/')?>",
            type  : "post",
            data  : {id:id},
            success : function(data){
               swal("Success!", "Batalkan!", "error");
               // window.location.reload();
            },
            error : function(data){
              swal("Error", "Your file not deleted","error");
            }
          });
    }

</script>