 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />   -->
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 <div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Invoice Approval Form</h6>
      </div>
      <div class="card-body">
        <input type="hidden" name="id_invoice" value="<?php echo $InvoiceData['status']?>">
        <form action="<?php echo base_url();?>Invoice/approve_invoice" method="post">
          <input type="hidden" name="id_invoice" value="<?php echo $InvoiceData['id_invoice']?>">
          <div class="form-group row" style="margin-bottom: 0px;">
            <label class="col-sm-3 col-form-label">Perusahaan </label>
            <label class="col-form-label">:</label>
            <div class="col-sm-5">
              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $InvoiceData['nama_perusahaan']?>">
            </div>
          </div>
          <div class="form-group row" style="margin-bottom: 0px;">
            <label class="col-sm-3 col-form-label">Port </label>
            <label class="col-form-label">:</label>
            <div class="col-sm-5">
              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $InvoiceData['nama_lokasi']?>">
            </div>
          </div>
          
          <div class="form-group row" style="margin-bottom: 0px;">
            <label class="col-sm-3 col-form-label">Mata Uang </label>
            <label class="col-form-label">:</label>
            <div class="col-sm-5">
              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo $InvoiceData['mata_uang']?>">
            </div>
          </div>
          
          <hr>
          <div class="form-group row" style="margin-bottom: 0px;">
            <label class="col-sm-3 col-form-label">Tanggal</label>
            <label class="col-form-label"></label>
            <div class="col-sm-5">
            </div>
          </div>
          <div class="form-group row" style="margin-bottom: 0px;">
            <label class="col-sm-2"></label>
            <label class="col-sm-2">Kirim</label>
            <label class="col-form-label">:</label>
            <div class="col-sm-5">
              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo date('d F Y', strtotime($InvoiceData['tanggal_kirim']))?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2"></label>
            <label class="col-sm-2">Bayar</label>
            <label class="col-form-label">:</label>
            <div class="col-sm-5">
              <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo date('d F Y', strtotime($InvoiceData['tanggal_bayar']))?>">
            </div>
          </div>
          <br>
          <div class="form-check">
             <?php if ($InvoiceData['status']==1){ 
              echo '<input class="form-check-input" type="checkbox" value="" id="checkbox">
                    <label class="form-check-label" for="defaultCheck1">
                     Apakah pengajuan ini ingin di Cancel/Batalkan, Aksi ini tidak bisa dikembalikan lagi.
                   </label>';
             }else{
               echo '<input class="form-check-input" type="checkbox" value="" id="checkbox">
                    <label class="form-check-label" for="defaultCheck1">
                     Dengan ini bersedia menindak lanjuti kegiatan di atas, Aksi ini tidak bisa dikembalikan lagi. 
                   </label>';
             }?>
         </div>
         <br>
         <div>
            <table class="table table-bordered">
              <tr>
                <th>No</th>
                <th>Kegiatan</th>
                <th>Unit</th>
                <th>Harga</th>
                <th>At Cost (%)</th>
              </tr>
              <?php 
              $no = 1;
              foreach ($InvoiceKegiatan as $value){?>      
                <tr>
                  <td><?php echo $no++ ?>.</td>
                  <td><?php echo $value->nama_kegiatan?></td>
                  <td><?php echo $value->unit?></td>
                  <td><?php echo number_format($value->harga, 2,',','.');?></td>
                  <td><?php echo number_format($value->at_cost, 2,',','.');?></td>
                </tr>
              <?php }?>
            </table>
          </div>
          <br>
         <div class="modal-footer">
          <a class="btn btn-secondary" href="<?php echo base_url();?>Invoice" style="width: 90px;">Kembali</a>
          <?php if ($InvoiceData['status']==1){
            echo '<input type="text" id="inp-ket" name="keterangan" class="form-control" placeholder="keterangan" required disabled>
                    <button name="approve_cancel" class="btn btn-danger" onclick="cancel_approve($CommercialData["id_commercial"])" style="width: 90px;color:white" id="btn_cancel" disabled>Cancel</button>';
          }else{
            echo '<button name="approve" class="btn btn-success" onclick="approve_data($InvoiceData["id_invoice"])" style="width: 90px;color:white" id="btn_approve" disabled>Approve</button>
                  <button name="cencel" class="btn btn-danger" onclick="cancel_data($InvoiceData["id_invoice"])" style="width: 90px;color:white" id="btn_cancel" disabled>Cancel</button>';
          }?>
        </div>
      </form>
    </div>
  </div>
</div>
</div>


<script type="text/javascript">
  function approve_data(id){
    $.ajax({
      url   : "<?php echo base_url('Invoice/detail_invoice/')?>",
      type  : "post",
      data  : {id:id},
      success : function(data){
       swal("Success!", "Berhasil Di Setujui!", "success");
               // window.location.reload();
             },
             error : function(data){
              swal("Error", "Your file not deleted","error");
            }
          });
  }

  function cancel_data(id){
    $.ajax({
      url   : "<?php echo base_url('Invoice/detail_invoice/')?>",
      type  : "post",
      data  : {id:id},
      success : function(data){
       swal("Success!", "Batalkan!", "error");
               // window.location.reload();
             },
             error : function(data){
              swal("Error", "Your file not deleted","error");
            }
          });
  }

  $('#checkbox').click(function(){
   var status_check = $(this).prop('checked');
    	// alert(status_check);
    	if (status_check) {
    		$('#btn_approve').prop('disabled', false);
        $('#btn_cancel').prop('disabled', false);
        $('#inp-ket').prop('disabled', false);
    	}else{
    		$('#btn_approve').prop('disabled', true);
    		$('#btn_cancel').prop('disabled', true);
    		$('#inp-ket').prop('disabled', true);
    	}
    });
  </script>