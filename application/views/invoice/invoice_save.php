<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />   -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Invoice</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>Invoice/save_invoice" method="post">
          <div class="form-group row" style="margin-bottom: 0px;">
            <div class="col-sm-4">
              <label class="control-label col-xs-3" >PDA NO</label>
              <select class="form-control pda_no" id="selectpicker" data-live-search="true">
                <option value="" disabled selected>PILIH</option>
                <?php foreach ($CommercialData as $value){ ?>
                  <option value="<?php echo $value->pda_no?>"><?php echo $value->pda_no?></option>
                <?php }?>
              </select>
            </div>   
            <div class="col-sm-2">
              <label>Tanggal Kirim :</label>
              <div class="form-group">
                <div class='input-group'>
                  <input class="form-control " id="tanggal-kirim" name="tanggal_kirim" placeholder="Tanggal Kirim" required>
                </div>
              </div>
            </div>
            <div class="col-sm-2">
              <label>Tanggal Bayar :</label>
              <div class="form-group">
                <div class='input-group'>
                  <input class="form-control " id="tanggal-bayar" name="tanggal_bayar" placeholder="Tanggal Bayar"> 
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <label>Term :</label>
              <div class="form-group">
                <div class='input-group'>
                  <input class="form-control" name="term" placeholder="Term"> 
                </div>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-5 ">
              <label for="perusahaan">Perusahaan :</label>
              <input type="hidden" class="form-control" name="id_perusahaan" readonly>
              <input type="text" class="form-control" name="nama_perusahaan" readonly>
            </div>
            <div class="col-sm-5 ">
             <label for="lokasi">Port :</label>
             <input type="hidden" class="form-control" name="id_lokasi" readonly>
             <input type="text" class="form-control" name="nama_lokasi" readonly>
           </div>
           <div class="col-sm-2">
            <label for="mata_uang">Mata Uang :</label>
            <input type="text" class="form-control" name="mata_uang" readonly>
          </div>
        </div>
            <input type="hidden" class="form-control" name="pda_no" readonly>
            <input type="hidden" class="form-control" name="id_commercial" readonly>
                <!-- <table class="" id="dynamic_field">  
                  <tr>
                    <td>Kegiatan :</td>
                    <td>Unit :</td>
                    <td>Harga :</td>
                    <td>At Cost (%) :</td>
                  </tr>
                  <tr>  
                    <td width="425">
                      <select type="text" name="id_kegiatan[]" class="form-control name_list" required>
                        <option value="">Pilih</option>
                        <?php foreach ($KegiatanData as $key => $value ) { ?>
                          <option value="<?php echo $value->id_kegiatan?>">
                            <?php echo $value->nama_kegiatan?>
                          </option>
                        <?php }?>
                      </select> 
                    </td> 
                    <td width="200"><input type="text" name="unit[]" placeholder="Unit" class="form-control name_list" required="" /></td>  
                    <td width="200"><input type="text" name="harga[]" placeholder="Harga" class="form-control name_list" required="" /></td>
                    <td width="200"><input type="text" name="at_cost[]" placeholder="at Cost (%)" class="form-control name_list" required="" /></td>   
                    <td><button type="button" name="add" id="add" class="btn btn-success"><i class="fas fa-plus"></i></button></td>  
                  </tr>  
                </table>   -->
                <br>

                <div class="modal-footer">
                  <a class="btn btn-secondary" href="<?php echo base_url();?>Invoice">Close</a>
                  <button type="submit" name="submit" class="btn btn-primary">Save</button>
                </div>
              </form>



            </div>
          </div>
        </div>
      </div>


      <script type="text/javascript">
        $(document).ready(function(){      
         //  var i=1;  

         //  $('#add').click(function(){  
         //   i++;  
         //   $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><select type="text" name="id_kegiatan[]" class="form-control name_list" required><option value="">Kegiatan</option><?php foreach ($KegiatanData as $key => $value ) { ?><option value="<?php echo $value->id_kegiatan?>"><?php echo $value->nama_kegiatan?></option><?php }?></select></td><td><input type="text" name="unit[]" placeholder="Unit" class="form-control name_list" required /></td><td><input type="text" name="harga[]" placeholder="Harga" class="form-control name_list" required /></td><td><input type="text" name="at_cost[]" placeholder="at Cost (%)" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class="fas fa-minus"></i></button></td></tr>');  
         // });

         //  $(document).on('click', '.btn_remove', function(){  
         //   var button_id = $(this).attr("id");   
         //   $('#row'+button_id+'').remove();  
         // });  



         $('.pda_no').on('change',function(){
          var pda_no=$(this).val();
          $.ajax({
            type  : "POST",
            url   : "<?php echo base_url('Invoice/cari_pda_no');?>",
            dataType  : "JSON",
            data  : {pda_no : pda_no},
            cache : false,
            success : function(data){
              $.each(data, function(id_commercial,pda_no,id_perusahaan, nama_perusahaan, id_lokasi, nama_lokasi, mata_uang){
                $('[name="id_commercial"]').val(data.id_commercial);
                $('[name="pda_no"]').val(data.pda_no);
                $('[name="id_perusahaan"]').val(data.id_perusahaan);
                $('[name="nama_perusahaan"]').val(data.nama_perusahaan);
                $('[name="id_lokasi"]').val(data.id_lokasi);
                $('[name="nama_lokasi"]').val(data.nama_lokasi);
                $('[name="mata_uang"]').val(data.mata_uang);
              });
            }
          });
          return false;
        });
       }); 
     </script>