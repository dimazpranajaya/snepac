<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table {
    border-collapse: collapse
}
td,th{
    border: 1px solid #00000
}
</style>
<body>
	<table width="100%">
	<thead>
		<tr>
			<th colspan="7" style="border: none;color: white">xxx</th>
		</tr>
		<tr>
			<th colspan="7" style="border: none; color: white">xxx</th>
		</tr>
		<tr>
			<th colspan="7" align="center" style="border: none;">Laporan Commercial PDF</th>
		</tr>
		<tr>
			<th colspan="7" style="border: none;color: white">xxx</th>
		</tr>
		<tr>
			<th colspan="7" style="border-left: none; border-right: none;border-top:none;color: white">xxx</th>
		</tr>
		<tr>
			<th align="center" style="border: 1px solid black;padding: 2px;">No</th>
			<th align="center" style="border: 1px solid black;padding: 2px;">Perusahaan</th>
			<th align="center" style="border: 1px solid black;padding: 2px;">Port</th>
			<th align="center" style="border: 1px solid black;padding: 2px;">Kegiatan</th>
			<th align="center" style="border: 1px solid black;padding: 2px;">Mata Uang</th>
			<th align="center" style="border: 1px solid black;padding: 2px;">Harga</th>
			<th align="center" style="border: 1px solid black;padding: 2px;">At Cost (%)</th>
		</tr>
	</thead>
	<tbody>
		<?php $i=1; foreach($CommercialData as $value) { ?>
			<tr>
				<td width="35" style="border: 1px solid black;padding: 2px;" align="center"><?php echo $i++; ?>.</td>
				<td width="220" style="border: 1px solid black;padding: 2px;" ><?php echo $value->nama_perusahaan ?></td>
				<td width="220" style="border: 1px solid black;padding: 2px;" ><?php echo $value->nama_lokasi ?></td>
				<td width="220" style="border: 1px solid black;padding: 2px;" ><?php echo $value->nama_kegiatan ?></td>
				<td width="80" style="border: 1px solid black;padding: 2px;" align="center"><?php echo $value->mata_uang ?></td>
				<td width="100" style="border: 1px solid black;padding: 2px;" align="right"><?php echo number_format($value->harga, 2, '.', ',') ?></td>
				<td width="100" style="border: 1px solid black;padding: 2px;" align="right"><?php echo $value->at_cost ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</body>
</html>