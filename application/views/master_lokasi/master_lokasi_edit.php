<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Port</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>MasterLokasi/edit_lokasi" method="post">
        <input type="hidden" id="id" name="id_lokasi" value="<?php echo $LokasiData['id_lokasi']?>">  
        <div class="container">
              <div class="form-group row">
                <!-- <label for="perusahaan">Perusahaan :</label> -->
                <!-- <select class="form-control" id="id_perusahaan" name="id_perusahaan">
                  <option value="" disabled selected="">Pilih</option>
                  <?php foreach ($PerusahaanData as $key => $value ) { ?>
                    <option value="<?php echo $value->id_perusahaan?>" <?php if ($value->id_perusahaan == $LokasiData['id_perusahaan']){ echo "selected";}?>>
                    <?php echo $value->nama_perusahaan?>
                    </option>
                  <?php }?>
                </select> -->
              </div>
              <div class="form-group row">
                <label for="nama_lokasi">Port :</label>
                <input type="text" class="form-control" id="nama_lokasi" name="nama_lokasi" value="<?php echo $LokasiData['nama_lokasi']?>">  
              </div>
              <div class="form-group row">
                <label for="ket_lokasi">Keterangan :</label>
                <textarea type="text" class="form-control" rows="3" id="ket_lokasi" name="ket_lokasi"><?php echo $LokasiData['ket_lokasi']?></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <a class="btn btn-secondary" href="<?php echo base_url();?>MasterLokasi">Close</a>
            <button type="submit" name="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
