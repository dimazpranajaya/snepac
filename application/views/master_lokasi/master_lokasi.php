 <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success!</strong> Item created successfully.
    </div>      
<?php } ?>
<?php if ($this->session->flashdata('update')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success!</strong> Item update successfully.
    </div>      
<?php } ?>
 <div class="card shadow mb-4">
  <div class="card-header py-3">
    <a href="#" style="float: right; margin-right: 5px;" class="btn btn-primary btn-icon-split btn-sm" data-toggle="modal" data-target="#ModalPerusahaan">
      <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
      </span>
      <span class="text">Add</span>
    </a>
    <h6 class="m-0 font-weight-bold text-primary">List Port</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <!-- <th>Perusahaan</th> -->
            <th>Port</th>
            <th>Keterangan</th>
            <th width="45" style="text-align: center;">Set</th>
          </tr>
        </thead>
        <tbody>
           <?php
            $no=1; 
            foreach ($LokasiData as $key => $value) { ?>
          <tr>
            <td><?php echo $no++?></td>
            <!-- <td><?php echo $value->nama_perusahaan?></td> -->
            <td><?php echo $value->nama_lokasi?></td>
            <td><?php echo $value->ket_lokasi?></td>
            <td style="text-align: center;">
                <a href="<?php echo base_url()?>MasterLokasi/edit_lokasi/<?php echo $value->id_lokasi?>" class="btn btn-xs btn-primary btn-circle btn-sm"><i class="fa fa-edit"></i></a>
                <a class="btn btn-xs btn-danger btn-circle btn-sm" onclick="delete_data(<?php echo $value->id_lokasi?>)"><i style="color: white;" class="fas fa-trash"></i></a>
            </td>
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="ModalPerusahaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="col-12">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"><b>Data Master</b> - Add Form</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form action="<?php echo base_url();?>MasterLokasi/save_lokasi" method="post">
        <div class="container">
              <div class="form-group row" >
                <!-- <div class="col-10">
                <label for="id_perusahaan">Perusahaan :</label>
                <select class="form-control" id="id_perusahaan" name="id_perusahaan">
                  <option value="" disabled selected="">Pilih</option>
                  <?php foreach ($PerusahaanData as $key => $value ) { ?>
                    <option value="<?php echo $value->id_perusahaan?>"><?php echo $value->nama_perusahaan?></option>
                  <?php }?>
                </select>
                </div> -->
                <!-- <div class="col-2">
                  <label style="color: white;">Add</label>
                  <a href="<?php echo base_url();?>MasterPerusahaan/save_perusahaan" class="btn btn-primary" >+</a>
                </div> -->
              </div>
              <div class="form-group">
                <label for="nama_lokasi">Lokasi :</label>
                <input type="text" class="form-control" id="nama_lokasi" placeholder="Lokasi" name="nama_lokasi" required>  
              </div>
              <div class="form-group">
                <label for="ket_lokasi">Keterangan :</label>
                <textarea type="text" class="form-control" rows="3" id="ket_lokasi" name="ket_lokasi" placeholder="Keterangan" required></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
    </div>
  </div>
<script type="text/javascript">
    function delete_data(id)
      {
        swal({
          title: "Are you sure?",
          text: "Your will not be able to recover this file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(){
          $.ajax({
            url   : "<?php echo base_url('MasterLokasi/delete_lokasi/')?>",
            type  : "post",
            data  : {id:id},
            success : function(){
              swal("Deleted!", "Your file has been deleted.", "success");
              window.location.reload();
            },
            error : function(){
              swal("gagal", "cdvsdvdsvdvf","error");
            }
          });
        });
      }
  </script>