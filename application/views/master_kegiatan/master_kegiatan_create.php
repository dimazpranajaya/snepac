<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Kegiatan</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>MasterKegiatan/save_kegiatan" method="post">
        <div class="container">
              <div class="form-group">
                <label for="perusahaan">Perusahaan :</label>
                <select class="form-control" id="id_perusahaan" name="id_perusahaan" required> 
                  <option value="" disabled selected="">Pilih</option>
                  <?php foreach ($PerusahaanData as $key => $value ) { ?>
                    <option value="<?php echo $value->id_perusahaan?>">
                    <?php echo $value->nama_perusahaan?>
                    </option>
                  <?php }?>
                </select>
              </div>
              <div class="form-group">
                <label for="lokasi">Port :</label>
                <select class="form-control" id="id_lokasi" name="id_lokasi" required>
                  <option value="" disabled selected="">Pilih</option>
                  <?php foreach ($LokasiData as $key => $value ) { ?>
                    <option value="<?php echo $value->id_lokasi?>">
                    <?php echo $value->nama_lokasi?>
                    </option>
                  <?php }?>
                </select>
              </div>
              <div class="form-group">
                <label for="kegiatan">Kegiatan :</label>
                <textarea type="text" class="form-control" rows="2" id="nama_kegiatan" name="nama_kegiatan" placeholder="Keterangan Kegiatan"></textarea>
              </div>
              <div class="form-group">
                <label for="keterangan">Keterangan :</label>
                <textarea type="text" class="form-control" rows="2" id="ket_kegiatan" name="ket_kegiatan" placeholder="keterangan"></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <a class="btn btn-secondary" href="<?php echo base_url();?>MasterKegiatan">Close</a>
            <button type="submit" name="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
