<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Edit</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>MasterKegiatan/edit_kegiatan" method="post">
        <input type="hidden" id="id" name="id" value="<?php echo $KegiatanData['id_kegiatan']?>">  
        <div class="container">
              <div class="form-group">
                <label for="id_perusahaan">Perusahaan :</label>
                <select class="form-control" id="id_perusahaan" name="id_perusahaan"> 
                  <option value="" disabled selected="">Pilih</option>
                  <?php foreach ($PerusahaanData as $key => $value ) { ?>
                    <option value="<?php echo $value->id_perusahaan?>" <?php if ($value->id_perusahaan == $KegiatanData['id_perusahaan']){ echo "selected";}?>>
                    <?php echo $value->nama_perusahaan?>
                    </option>
                  <?php }?>
                </select>
              </div>
              <div class="form-group">
                <label for="id_lokasi">Port :</label>
                <select class="form-control" id="id_lokasi" name="id_lokasi"> 
                  <option value="" disabled selected="">Pilih</option>
                  <?php foreach ($LokasiData as $key => $value ) { ?>
                    <option value="<?php echo $value->id_lokasi?>" <?php if ($value->id_lokasi == $KegiatanData['id_lokasi']){ echo "selected";}?>>
                    <?php echo $value->nama_lokasi?>
                    </option>
                  <?php }?>
                </select>
              </div>
              <div class="form-group">
                <label for="kegiatan">Kegiatan :</label>
                <textarea type="text" class="form-control" rows="2" id="nama_kegiatan" name="nama_kegiatan"><?php echo $KegiatanData['nama_kegiatan']?></textarea>
              </div>
              <div class="form-group">
                <label for="keterangan">Keterangan :</label>
                <textarea type="text" class="form-control" rows="2" id="ket_kegiatan" name="ket_kegiatan"><?php echo $KegiatanData['ket_kegiatan']?></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <a class="btn btn-secondary" href="<?php echo base_url();?>MasterKegiatan">Close</a>
            <button type="submit" name="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
