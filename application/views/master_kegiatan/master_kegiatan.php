 <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-info alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success!</strong> Item created successfully.
    </div>      
<?php } ?>
<?php if ($this->session->flashdata('update')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success!</strong> Item update successfully.
    </div>      
<?php } ?>
 <div class="card shadow mb-4">
  <div class="card-header py-3">
    <a href="<?php echo base_url();?>MasterKegiatan/save_kegiatan" style="float: right; margin-right: 5px;" class="btn btn-primary btn-icon-split btn-sm">
      <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
      </span>
      <span class="text">Add</span>
    </a>
    <h6 class="m-0 font-weight-bold text-primary">List Port</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Perusahaan</th>
            <th>Port</th>
            <th>Kegiatan</th>
            <th>Keterangan</th>
            <th width="45" style="text-align: center;">Set</th>
          </tr>
        </thead>
        <tbody>
           <?php
            $no=1; 
            foreach ($KegiatanData as $key => $value) { ?>
          <tr>
            <td><?php echo $no++?></td>
            <td><?php echo $value->nama_perusahaan?></td>
            <td><?php echo $value->nama_lokasi?></td>
            <td><?php echo $value->nama_kegiatan?></td>
            <td><?php echo $value->ket_kegiatan?></td>
            <td style="text-align: center;">
                <a href="<?php echo base_url()?>MasterKegiatan/edit_kegiatan/<?php echo $value->id_kegiatan?>" class="btn btn-xs btn-primary btn-circle btn-sm"><i class="fa fa-edit"></i></a>
                <a class="btn btn-xs btn-danger btn-circle btn-sm" onclick="delete_data(<?php echo $value->id_kegiatan?>)"><i class="fas fa-trash"></i></a>
            </td>
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
    function delete_data(id)
      {
        swal({
          title: "Are you sure?",
          text: "Your will not be able to recover this file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(){
          $.ajax({
            url   : "<?php echo base_url('MasterKegiatan/delete_kegiatan/')?>",
            type  : "post",
            data  : {id:id},
            success : function(){
              swal("Deleted!", "Your file has been deleted.", "success");
              window.location.reload();
            },
            error : function(){
              swal("gagal", "cdvsdvdsvdvf","error");
            }
          });
        });
      }
  </script>