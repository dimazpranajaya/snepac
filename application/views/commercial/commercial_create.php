 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />   -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="row">
  <div class="col-lg-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form Commercial</h6>
      </div>
      <div class="card-body">
        <form action="<?php echo base_url();?>Commercial/save_commercial" method="post">
          <div class="form-group row" style="margin-bottom: 0px;">
            <div class="col-sm-3">
              <label for="tanggal">Tanggal :</label>
              <div class="form-group">
              <div class='input-group'>
                  <input autocomplete="off" size="16" class="form-control" id="datepicker" name="tanggal" placeholder="Tanggal" value="<?php echo date("d F Y");?>">
                  <!-- <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span> -->
              </div>
              </div>
            </div>
            <div class="col-sm-5 ">
              <label for="perusahaan">Perusahaan :</label>
                <select class="form-control" id="id_perusahaan" name="id_perusahaan" required>  
                  <option value="">Pilih</option>
                  <?php foreach ($PerusahaanData as $key => $value ) { ?>
                    <option value="<?php echo $value->id_perusahaan?>">
                    <?php echo $value->nama_perusahaan?>
                    </option>
                  <?php }?>
                </select>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-4">
               <label for="lokasi">Port :</label>
                <select class="form-control" id="id_lokasi" name="id_lokasi" required>
                  <option value="">Pilih</option>
                  <?php foreach ($LokasiData as $key => $value ) { ?>
                    <!-- <option id="id_lokasi" class="<?php echo $value->id_perusahaan?>" value="<?php echo $value->id_lokasi?>"> -->
                    <option id="id_lokasi"  value="<?php echo $value->id_lokasi?>">
                    <?php echo $value->nama_lokasi?>
                    </option>
                  <?php }?>
                </select>
            </div>
            <div class="col-sm-5 ">
              <label for="kapal">Kapal :</label>
                <select class="form-control" id="id_kapal" name="id_kapal">  
                  <option value="">Pilih</option>
                  <?php foreach ($KapalData as $key => $value ) { ?>
                    <!-- <option id="id_kapal" class="<?php echo $value->id_perusahaan?>" value="<?php echo $value->id_kapal?>"> -->
                    <option id="id_kapal" value="<?php echo $value->id_kapal?>">
                    <?php echo $value->nama_kapal?>
                    </option>
                  <?php }?>
                </select>
            </div>
            <!-- <input type="text" name="kode_perusahaan" value="<?php echo $value->kode_perusahaan?>"> -->
            <div class="col-sm-3">
              <label for="mata_uang">Mata Uang :</label>
              <select class="form-control" id="mata_uang" name="mata_uang" required>
                <!-- <option value="" disabled selected="">Pilih</option> -->
                <option value="IDR" selected="">IDR</option>
                <option value="SGD">SGD</option>
              </select>
            </div>
          </div>

     <!--      <div class="form-group row" id="dynamic_field">
            <div class="col-lg-4">
            <label for="nama_group">Nama Group :</label>
            <input type="text" name="addmore[][name]" placeholder="Enter your Name" class="form-control name_list" required>  
          </div>
            <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
          </div> -->
  
        <!-- <div class="table-responsive">   -->
                <table class="" id="dynamic_field">  
                    <tr>
                      <td>Kegiatan :</td>
                      <td>Unit :</td>
                      <td>Harga :</td>
                      <td>Harga Dasar: (PDA)</td>
                      <td>At Cost (%) :</td>
                      <td>Keterangan :</td>
                    </tr>
                    <tr>  
                        <td width="425">
                          <select type="text" name="id_kegiatan[]" class="form-control name_list" required>
                              <option value="">Pilih</option>
                              <?php foreach ($KegiatanData as $key => $value ) { ?>
                                <option value="<?php echo $value->id_kegiatan?>">
                                <?php echo $value->nama_kegiatan?>
                                </option>
                              <?php }?>
                          </select> 
                        </td> 
                        <td width="200"><input type="number" name="unit[]" placeholder="Unit" class="form-control name_list" required="" /></td>  
                        <td width="200"><input type="number" name="harga[]" placeholder="Harga" class="form-control name_list" required="" /></td>
                        <td width="200"><input type="number" name="harga_dasar[]" placeholder="Harga Dasar" class="form-control name_list" required="" /></td>
                        <td width="200"><input type="number" name="at_cost[]" placeholder="at Cost (%)" class="form-control name_list" required="" value=""/></td>   
                        <td width="300"><input type="text" name="ket_kegiatan[]" placeholder="Keterangan" class="form-control name_list" required="" /></td>   
                        <td><button type="button" name="add" id="add" class="btn btn-success"><i class="fas fa-plus"></i></button></td>  
                    </tr>  
                </table>  
                <!-- <input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit" />   -->
            <!-- </div> -->
            <br>
          
          <div class="modal-footer">
            <a class="btn btn-secondary" href="<?php echo base_url();?>Commercial">Close</a>
            <button type="submit" name="submit" class="btn btn-primary">Save</button>
          </div>
        </form>



      </div>
    </div>
  </div>
</div>

   
<script type="text/javascript">
    $(document).ready(function(){      
      var i=1;  
   
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">         <td><select type="text" name="id_kegiatan[]" class="form-control name_list" required><option value="">Pilih</option><?php foreach ($KegiatanData as $key => $value ) { ?><option value="<?php echo $value->id_kegiatan?>"><?php echo $value->nama_kegiatan?></option><?php }?></select></td>      <td><input type="number" name="unit[]" placeholder="Unit" class="form-control name_list" required /></td>                     <td><input type="number" name="harga[]" placeholder="Harga" class="form-control name_list" required /></td>                    <td><input type="number" name="harga_dasar[]" placeholder="Harga Dasar" class="form-control name_list" required /></td>                    <td><input type="number" name="at_cost[]" placeholder="at Cost (%)" class="form-control name_list" value="" required /></td>              <td><input type="text" name="ket_kegiatan[]" placeholder="Keterangan" class="form-control name_list" required /></td>               <td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class="fas fa-minus"></i></button></td></tr>');  
      });
  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
  
    });  
</script>