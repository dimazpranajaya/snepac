 <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success!</strong> Item created successfully.
    </div>      
<?php } ?>
<?php if ($this->session->flashdata('update')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success!</strong> Item update successfully.
    </div>      
<?php } ?>
 <div class="card shadow mb-4">
  <div class="card-header py-3">
    
    <a href="<?php echo base_url();?>Commercial/save_commercial" style="float: right; margin-right: 5px;" class="btn btn-primary btn-icon-split btn-sm">
      <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
      </span>
      <span class="text">Add</span>
    </a>
    <a href="<?php echo base_url();?>Commercial/report_pdf" style="float: right; margin-right: 5px;" class="btn btn-danger btn-icon-split btn-sm" target="__blank">
      <span class="icon text-white-50">
        <i class="fas fa-file-pdf"></i>
      </span>
      <span class="text">Pdf</span>
    </a>
    <a href="<?php echo base_url();?>Commercial/report_excel" style="float: right; margin-right: 5px;" class="btn btn-info btn-icon-split btn-sm">
      <span class="icon text-white-50">
        <i class="fas fa-file-excel"></i>
      </span>
      <span class="text">Excel</span>
    </a>
    <h6 class="m-0 font-weight-bold text-primary">List Commercial</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>PDA No</th>
            <th>Perusahaan</th>
            <th>Port</th>
            <th>Kapal</th>
            <th>Mata Uang</th>
            <th>Nilai PDA</th> <!-- ini adalah Harga Dasar -->
            <th width="80" style="text-align: center;">Status</th>
            <th width="45" style="text-align: center;">Set</th>
          </tr>
        </thead>
        <tbody>
           <?php
            $no=1; 
            foreach ($CommercialData as $key => $value) { ?>
          <tr>
            <td><?php echo $no++?></td>
            <td>
              <?php if ($value->status == 1){
                echo "<a href='Commercial/detail_commercial/$value->id_commercial'>$value->pda_no</a>";
              }else{
                echo "$value->pda_no";
              }?>
            </td>
            <td><?php echo $value->nama_perusahaan?></td>
            <td><?php echo $value->nama_lokasi?></td>
            <td><?php echo $value->nama_kapal?></td>
            <td><?php echo $value->mata_uang?></td>
            <td><?php echo number_format($value->total_harga_dasar, 2,',','.');?></td>
            <td style="text-align: center;">
              <?php if ($AdminData == 2 && $value->status == 0){
                echo '<a class="m-0 font-weight-bold text-primary">OPEN</a>';
              }else if ($value->status == 0){
                echo "<a href='Commercial/approve_commercial/$value->id_commercial' class='m-0 font-weight-bold text-primary'>OPEN</a>";
              }else if ($AdminData == 3 && $value->status == 1){
                echo "<a href='Commercial/approve_commercial/$value->id_commercial' class='m-0 font-weight-bold text-success'>APPROVED</a>";
              }else if ($value->status == 1){
                echo "<a class='m-0 font-weight-bold text-success'>APPROVED</a>";
              }else if ($value->status == 2){
                echo '<a class="m-0 font-weight-bold text-danger">CANCEL</a>';
              }else{
                echo "<a class='m-0 font-weight-bold text-danger'>RAJECT</a>";
              } 
              ?>
            </td>
            <td style="text-align: center;">              
              <?php if (($AdminData == 2 or $AdminData == 1) && $value->status == 0){    // staff & open
                echo "<a href='Commercial/edit_commercial/$value->id_commercial' class='btn btn-xs btn-primary btn-circle btn-sm' ><i class='fa fa-edit'></i></a>";
                echo "<a class='btn btn-xs btn-danger btn-circle btn-sm' onclick='delete_data($value->id_commercial)'><i class='fas fa-trash' style='color: white;'></i></a>";
              }else if ($AdminData == 1 && $value->status == 1){    // admin & approved
                echo "<a class='btn btn-xs btn-primary btn-circle btn-sm'><i class='fa fa-edit'></i></a>";
                echo "<a class='btn btn-xs btn-danger btn-circle btn-sm'><i class='fas fa-trash'></i></a>";
              }else if ($AdminData == 3) {
                echo "<a class='btn btn-xs btn-primary btn-circle btn-sm'><i class='fa fa-edit'></i></a>";
                echo "<a class='btn btn-xs btn-danger btn-circle btn-sm'><i class='fas fa-trash'></i></a>";
              }else{
                echo "<a class='btn btn-xs btn-primary btn-circle btn-sm'><i class='fa fa-edit'></i></a>";
                echo "<a class='btn btn-xs btn-danger btn-circle btn-sm'><i class='fas fa-trash'></i></a>";
              }
              ?>
            </td>
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
    function delete_data(id)
      {
        swal({
          title: "Are you sure?",
          text: "Your will not be able to recover this file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(){
          $.ajax({
            url   : "<?php echo base_url('Commercial/delete_commercial/')?>",
            type  : "post",
            data  : {id:id},
            success : function(){
              swal("Deleted!", "Your file has been deleted.", "success");
              window.location.reload();
            },
            error : function(){
              swal("Error", "Your file not deleted","error");
            }
          });
        });
      }


  </script>