<style type="text/css">
	table {
  		border-collapse: collapse;
	}
	table, th, td {
	 	border: 1px solid black;
	 	font-size: 14px;
	 	font-family:Calibri (Body);
	}
</style>
<p style="font-size: 22px;font-weight: bold;text-align: center;line-height: 5%; ">KEMENTRIAN SOSIAL REPUBLIK INDONESIA</p>
<p style="font-size: 16px;text-align: center;line-height:-15%; ">Jalan Salemba Raya Nomor 28 Jakarta Pusat 10430</p>
<p style="font-size: 16px;text-align: center;">Telp. 021-3160065/ 3100109 ext. 2420 Fax. 021-3103677 Laman <u>www.kemsos.go.id</u></p>
<hr/ size="4px" line>
<table style="border: none">
	<tr>
		<td style="border: none;padding-left: 25px;">Nomor </td>
		<td style="border: none">: </td>
		<td style="border: none">20/4.1/KP.0405/01/2019</td>
	</tr>
	<tr>
		<td style="border: none;padding-left: 25px;">Lampiran</td>
		<td style="border: none">: </td>
	</tr>
	<tr>
		<td style="border: none;padding-left: 25px;">Hal</td>
		<td style="border: none">: </td>
		<td style="border: none">Usulan Kenaikan Pangkat</td>
	</tr>
	<tr>
		<td style="border: none;padding-left: 25px;"></td>
		<td style="border: none;"></td>
		<td style="border: none;">a.n Dra. Dewi Suhartini, M.Si</td>
	</tr>
	<tr>
		<td style="border: none;padding-left: 25px; padding-bottom: 5em;" colspan="3">
		</td>
	</tr>
	<tr>
		<td style="border: none;padding-left: 25px;" colspan="3">
			Yth, Kepala Biro Organiasi dan Kepegawaian<br>
			Kementrian Sosial RI <br>
			di - <br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jakarta
		</td>
	</tr>
</table>
<br>
<br>
<br>
<br>
<table style="border:none;">
	<tr>
		<td style="padding:2px;border:none;padding-left: 25px;"></td>
		<td style="padding:2px;width:35px;" align="center">NO</td>
		<td style="padding:2px;width:550px;" colspan="3" align="center">DATA PEGAWAI NEGERI SIPIL</td>
	</tr>
	<tr>
		<td style="padding:2px;border:none;padding-left: 25px;"></td>
		<td style="padding:2px;width:35px;" align="center">1.</td>
		<td style="padding:2px;width:40px;">Nama</td>
		<td style="padding:2px;width:40px;">Dra. Dewi Suhartini, M.Si</td>
		<td style="padding:2px;width:40px;">Dra. Dewi Suhartini, M.Si</td>
	</tr>
	<tr>
		<td style="padding:2px;border:none;padding-left: 25px;"></td>
		<td style="padding:2px;width:35px;" align="center">2.</td>
		<td style="padding:2px;width:40px;">NIP</td>
		<td style="padding:2px;width:40px;">19680421212121212</td>
		<td style="padding:2px;width:40px;">19680421212121212</td>
	</tr>
	<tr>
		<td style="padding:2px;border:none;padding-left: 25px;"></td>
		<td style="padding:2px;width:35px;" align="center">3.</td>
		<td style="padding:2px;width:40px;">Pendidikan Terakhir (Bidang Studi)</td>
		<td style="padding:2px;width:40px;">Magister Ilmu Administrasi</td>
		<td style="padding:2px;width:40px;">Magister Ilmu Administrasi</td>
	</tr>
	<tr>
		<td style="padding:2px;border:none;padding-left: 25px;"></td>
		<td style="padding:2px;width:35px;" align="center">4.</td>
		<td style="padding:2px;width:40px;">Tempat / Tanggal Lahir</td>
		<td style="padding:2px;width:40px;">Bangka, 21 April 1968</td>
		<td style="padding:2px;width:40px;">Bangka, 21 April 1968</td>
	</tr>
	
</table>