<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table width="100%">
	<thead>
		<tr>
			<th></th>
			<th colspan="6"></th>
		</tr>
		<tr>
			<th></th>
			<th colspan="6">Laporan Commercial Excel</th>
		</tr>
		<tr>
			<th></th>
			<th colspan="6"></th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$i=1; 
		$id_commercial='';
		foreach($CommercialData as $value) { 
			if ($value['id_commercial'] != $id_commercial){
			$id_commercial =$value['id_commercial'];
			echo "<tr>
					<th width='100'></th>
					<th colspan='6' align='left' style='border:0.1px solid black:'>".$value['nama_perusahaan']."</th>
				 </tr>";

			echo "<tr>
					<th width='100'></th>
					<th style='border:0.1px solid black:'>No</th>
					<th style='border:0.1px solid black:'>Port</th>
					<th style='border:0.1px solid black:'>Nama Kegiatan</th>
					<th style='border:0.1px solid black:'>Mata Uang</th>
					<th style='border:0.1px solid black:'>Harga Dasar</th>
					<th style='border:0.1px solid black:'>At Cost</th>
				</tr>";

			echo "<tr>
				<td width='100'></td>
				<td width='30' align='center' style='border:0.1px solid black:'>".$i++." .</td>
				<td width='250' style='border:0.1px solid black:'>".$value['nama_lokasi']."</td>
				<td width='250' style='border:0.1px solid black:'>".$value['nama_kegiatan']."</td>
				<td width='100' align='center' style='border:0.1px solid black:'>".$value['mata_uang']."</td>
				<td width='80' align='right' style='border:0.1px solid black:'>".$value['harga']."</td>
				<td width='80' align='right' style='border:0.1px solid black:'>".$value['at_cost']."</td>
			</tr>";
			}else{
				echo "<tr>
					<td width='100'></td>
					<td width='30' align='center' style='border:0.1px solid black:'>".$i++." .</td>
					<td width='250' style='border:0.1px solid black:'>".$value['nama_lokasi']."</td>
					<td width='250' style='border:0.1px solid black:'>".$value['nama_kegiatan']."</td>
					<td width='100' align='center' style='border:0.1px solid black:'>".$value['mata_uang']."</td>
					<td width='80' align='right' style='border:0.1px solid black:'>".$value['harga']."</td>
					<td width='80' align='right' style='border:0.1px solid black:'>".$value['at_cost']."</td>
				</tr>";
			}?>					
			<?php } ?>
		</tbody>
	</table>