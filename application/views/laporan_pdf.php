<?php
	$contenct =
	"<style>
		table {
		  font-family: arial, sans-serif;
		  border-collapse: collapse;
		  width: 50%;
		}

		td, th {
		  /*border: 1px solid #dddddd;*/
		  text-align: left;
		  padding: 3px;
		}

		tr:nth-child(even) {
		  background-color: #dddddd;
		}
	</style>";

		$contenct .="<page>
					<div>
					<br>
					<img width='1070' height='120' src='./assets/img/header.jpg'>
					<br>
					<br>
						<h2 style='text-align:center;text-decoration:underline'>Commercial</h2>
					<br>
					<table>"; 
		$i=1;
		$id_commercial='';
		foreach ($CommercialData as $value) {
		if ($value['id_commercial'] != $id_commercial) {	
		$id_commercial = $value['id_commercial'];   
		$contenct .= "<tr>
						<td colspan='7' width='240' style='border: 1px solid black;padding: 2px;font-weight: bold;font-size:18;' >".$value['nama_perusahaan']."</td>
		  			</tr>";								
		$contenct .="<tr>
					<tH width='35' style='border: 1px solid black;padding: 2px;' align='center'>No</tH>
					<tH width='320' style='border: 1px solid black;padding: 2px;' >Port</tH>
					<tH width='375' style='border: 1px solid black;padding: 2px;' >Nama Kegiatan</tH>
					<tH width='80' style='border: 1px solid black;padding: 2px;' align='center'>Mata Uang</tH>
					<tH width='100' style='border: 1px solid black;padding: 2px;' align='center'>Harga</tH>
					<tH width='100' style='border: 1px solid black;padding: 2px;' align='center'>At Cost</tH>			
					</tr>";
			}		
		$contenct .= "<tr>
			<td width='35' style='border: 1px solid black;padding: 2px;' align='center'>".$i++.".</td>
			<td width='320' style='border: 1px solid black;padding: 2px;' >".$value['nama_lokasi']."</td>
			<td width='375' style='border: 1px solid black;padding: 2px;' >".$value['nama_kegiatan']."</td>
			<td width='80' style='border: 1px solid black;padding: 2px;' align='center'>".$value['mata_uang']."</td>
			<td width='100' style='border: 1px solid black;padding: 2px;' align='right'>".number_format($value['harga'], 2, '.', ',')."</td>
			<td width='100' style='border: 1px solid black;padding: 2px;' align='right'>".number_format($value['at_cost'], 2, '.', ',')."</td>
			</tr>";
		}

		$contenct.="</table>
					</div>
			</page>";
	// var_dump($contenct);exit();
	require_once('assets/html2pdf/html2pdf.class.php');
	$html2pdf = new html2pdf('L', 'A4', 'en');
	$html2pdf->writeHTML($contenct);
	$html2pdf->Output('Laporan_Commercial_PDF.pdf');
 ?>