-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 16, 2020 at 09:23 PM
-- Server version: 5.7.29
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snepacco_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_activity`
--

CREATE TABLE `log_activity` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `activity_name` varchar(150) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_group`
--

CREATE TABLE `m_group` (
  `id_group` int(11) NOT NULL,
  `nama_group` varchar(100) NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_group`
--

INSERT INTO `m_group` (`id_group`, `nama_group`, `keterangan`) VALUES
(9, 'Shipping Agency', 'SA'),
(10, 'Government Tariff', 'Tarif Pemerintah'),
(11, 'Entry Permit', '-'),
(13, 'Vessel Expenses', '-'),
(14, 'Crew Formalities', '-'),
(15, 'Stevedorin', '-'),
(16, 'Transportation', 'traportasi');

-- --------------------------------------------------------

--
-- Table structure for table `m_kapal`
--

CREATE TABLE `m_kapal` (
  `id_kapal` int(5) NOT NULL,
  `id_perusahaan` int(5) DEFAULT NULL,
  `nama_kapal` varchar(255) DEFAULT NULL,
  `imo` varchar(255) DEFAULT NULL,
  `grt` decimal(50,0) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kapal`
--

INSERT INTO `m_kapal` (`id_kapal`, `id_perusahaan`, `nama_kapal`, `imo`, `grt`, `keterangan`) VALUES
(3, 5, 'Ship 1', '1', 1, 'test'),
(5, 6, 'MV. Candi Sewu', '7900467', 220, 'Khusus Carter'),
(6, 6, 'MT. Tanah Abang', '847521', 4500, 'Khusus Bunker'),
(7, 7, 'Ship X', '1', 1, '-');

-- --------------------------------------------------------

--
-- Table structure for table `m_kegiatan`
--

CREATE TABLE `m_kegiatan` (
  `id_kegiatan` int(5) NOT NULL,
  `id_group` int(10) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `ket_kegiatan` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kegiatan`
--

INSERT INTO `m_kegiatan` (`id_kegiatan`, `id_group`, `nama_kegiatan`, `ket_kegiatan`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(27, 9, 'Handling fee inwalk', '-', NULL, NULL, NULL, NULL),
(28, 9, 'Agency fee inward', '-', NULL, NULL, NULL, NULL),
(29, 9, 'Handling fee free pratique', '-', NULL, NULL, NULL, NULL),
(30, 9, 'Handling fee pilot permiun arrival', '', NULL, NULL, NULL, NULL),
(31, 10, 'Anchorage ues (government tariff + add cost (5%)', '-', NULL, NULL, NULL, NULL),
(32, 10, 'Harbour Dues', '-', NULL, NULL, NULL, NULL),
(33, 11, 'Handling fee PKKA', '-', NULL, NULL, NULL, NULL),
(34, 13, 'Handling fee hot work permit', '', NULL, NULL, NULL, NULL),
(35, 13, 'Escort coast guard for supervision the hot work permit', '', NULL, NULL, NULL, NULL),
(36, 13, 'Handling fee submerging permit', '', NULL, NULL, NULL, NULL),
(37, 14, 'Handling fee protection letter / wasbakum', '-', NULL, NULL, NULL, NULL),
(38, 14, 'Handling fee EPO (Exit Permit Only)', '', NULL, NULL, NULL, NULL),
(39, 14, 'Handling fee immigration escourt', '-', NULL, NULL, NULL, NULL),
(40, 15, 'Stevedooring permint', '-', NULL, NULL, NULL, NULL),
(41, 16, 'Land Transport', '-', NULL, NULL, NULL, NULL),
(42, 16, 'Boat Service', '-', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_lokasi`
--

CREATE TABLE `m_lokasi` (
  `id_lokasi` int(5) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `ket_lokasi` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_lokasi`
--

INSERT INTO `m_lokasi` (`id_lokasi`, `nama_lokasi`, `ket_lokasi`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(29, 'Batam Yard', '-', '2020-02-14 23:44:44', 1, '2020-02-14 23:45:00', 1),
(30, 'Batam 1', '-', '2020-02-15 07:12:33', 1, '2020-02-15 07:12:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_perusahaan`
--

CREATE TABLE `m_perusahaan` (
  `id_perusahaan` int(5) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `nama_owner` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(10) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `nama_kapal` varchar(255) DEFAULT NULL,
  `ket_perusahaan` text,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_perusahaan`
--

INSERT INTO `m_perusahaan` (`id_perusahaan`, `nama_perusahaan`, `nama_owner`, `kode_perusahaan`, `alamat`, `nama_kapal`, `ket_perusahaan`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(2, 'Triumph BV', 'Mr. Owner', 'DW', 'Anglo Eastern, Australia', 'MV. Ferry Ship', '-', '2020-01-05 23:11:25', 1, '2020-01-05 23:11:25', 1),
(3, 'CHUAN HUP OFFSHORE PTE LTD', 'CHO', 'CHO', 'SINGAPORE', 'ZIRCON', 'LN', '2020-01-09 15:25:05', 2, '2020-01-09 15:25:05', 2),
(4, 'RK OFFSHORE PTE LTD', 'CAPT HUSIN (PIC)', 'RKO', 'SINGAPORE', 'CREST ANGELICA', 'LN', '2020-02-05 09:08:11', 1, '2020-02-05 09:08:11', 1),
(5, 'TESTCOMPANY', 'Mr. Owner', 'TST', 'Jl. Kapten Tandean No. 01', NULL, '-', '2020-02-05 09:53:09', 1, '2020-02-05 10:58:30', 1),
(6, 'Edwin Jaya, Pte Ltd', 'Owner', 'EDP', 'Jl. Duyung, Komp. Citra Permai, Blok B No. 9, Sei Jodoh, Batam 29432', NULL, 'Tidak Ada', '2020-02-05 11:03:24', 1, '2020-02-05 11:04:04', 1),
(7, 'COMPANYX', 'Co', 'XX', 'Jl. Merdeka', NULL, '-', '2020-02-15 07:29:43', 1, '2020-02-15 07:29:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_commercial`
--

CREATE TABLE `t_commercial` (
  `id_commercial` int(5) NOT NULL,
  `pda_no` varchar(30) NOT NULL,
  `id_perusahaan` int(10) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `id_lokasi` int(10) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `id_kapal` int(5) DEFAULT NULL,
  `nama_kapal` varchar(100) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `mata_uang` varchar(5) NOT NULL,
  `keterangan` text NOT NULL,
  `status` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='PDA No adalah Dokumen Commercial, yang terbuat secara otomatis ketika simpan form commercial.\r\n\r\nCth : 025/DW/PDA/2019\r\n[auto_increment]/[kode_perusahaan]/[PDA]/[current_year]';

--
-- Dumping data for table `t_commercial`
--

INSERT INTO `t_commercial` (`id_commercial`, `pda_no`, `id_perusahaan`, `nama_perusahaan`, `id_lokasi`, `nama_lokasi`, `id_kapal`, `nama_kapal`, `tanggal`, `mata_uang`, `keterangan`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(260, '1/TST/PDA/2020', 5, 'TESTCOMPANY', 29, 'Batam Yard', NULL, NULL, '2020-02-15 00:00:00', 'IDR', '', 0, '2020-02-15 08:04:11', 1, '2020-02-15 08:04:11', 1),
(261, '261/XX/PDA/2020', 7, 'COMPANYX', 30, 'Batam 1', NULL, NULL, '2020-02-15 00:00:00', 'IDR', '', 1, '2020-02-15 13:58:15', 1, '2020-02-15 14:00:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_commercial_kegiatan`
--

CREATE TABLE `t_commercial_kegiatan` (
  `id_commercial_kegiatan` int(11) NOT NULL,
  `id_commercial` int(11) DEFAULT NULL,
  `id_kegiatan` int(11) DEFAULT NULL,
  `nama_kegiatan` varchar(255) DEFAULT NULL,
  `unit` int(5) DEFAULT NULL,
  `harga` decimal(12,2) DEFAULT NULL,
  `harga_dasar` decimal(12,2) DEFAULT NULL,
  `at_cost` decimal(12,2) DEFAULT NULL,
  `ket_kegiatan` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_commercial_kegiatan`
--

INSERT INTO `t_commercial_kegiatan` (`id_commercial_kegiatan`, `id_commercial`, `id_kegiatan`, `nama_kegiatan`, `unit`, `harga`, `harga_dasar`, `at_cost`, `ket_kegiatan`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(74, 260, 39, 'Handling fee immigration escourt', 1, 1.00, 1.00, 1.00, '-', '2020-02-15 08:04:11', 1, '2020-02-15 08:04:11', 1),
(75, 261, 27, 'Handling fee inwalk', 5, 7.00, 5.00, 4.00, '111', '2020-02-15 13:58:15', 1, '2020-02-15 13:58:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_invoice`
--

CREATE TABLE `t_invoice` (
  `id_invoice` int(5) NOT NULL,
  `no_invoice` varchar(100) NOT NULL,
  `id_perusahaan` int(5) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `id_lokasi` int(5) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `mata_uang` varchar(15) DEFAULT NULL,
  `keterangan` text,
  `tanggal_kirim` datetime DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `term` varchar(20) DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  `ket` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_invoice`
--

INSERT INTO `t_invoice` (`id_invoice`, `no_invoice`, `id_perusahaan`, `nama_perusahaan`, `id_lokasi`, `nama_lokasi`, `mata_uang`, `keterangan`, `tanggal_kirim`, `tanggal_bayar`, `term`, `status`, `ket`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(6, '1/XX/INV/02/2020', 7, 'COMPANYX', 30, 'Batam 1', 'IDR', NULL, '2020-01-14 00:00:00', '2020-01-23 00:00:00', '1212', 1, '', '2020-02-15 14:01:39', 1, '2020-02-15 14:04:04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_invoice_kegiatan`
--

CREATE TABLE `t_invoice_kegiatan` (
  `id_invoice_kegiatan` int(11) NOT NULL,
  `id_invoice` int(11) DEFAULT NULL,
  `id_kegiatan` int(11) DEFAULT NULL,
  `nama_kegiatan` varchar(255) DEFAULT NULL,
  `unit` int(5) DEFAULT NULL,
  `harga` decimal(12,2) DEFAULT NULL,
  `at_cost` decimal(12,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_invoice_kegiatan`
--

INSERT INTO `t_invoice_kegiatan` (`id_invoice_kegiatan`, `id_invoice`, `id_kegiatan`, `nama_kegiatan`, `unit`, `harga`, `at_cost`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(48, 6, 27, 'Handling fee inwalk', 5, 7.00, 4.00, '2020-02-15 14:01:39', 1, '2020-02-15 14:01:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` enum('0','1','2','3','4','5') NOT NULL,
  `last_login` datetime NOT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `last_login`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', '2019-11-25 18:48:30', 'admin'),
(2, 'commercial', '21232f297a57a5a743894a0e4a801fc3', '2', '0000-00-00 00:00:00', 'staff commercial'),
(3, 'commercialmanager', '21232f297a57a5a743894a0e4a801fc3', '3', '0000-00-00 00:00:00', 'manager commercial'),
(4, 'finance', '21232f297a57a5a743894a0e4a801fc3', '4', '0000-00-00 00:00:00', 'staff finance'),
(5, 'financemanager', '21232f297a57a5a743894a0e4a801fc3', '5', '0000-00-00 00:00:00', 'manager finance');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_activity`
--
ALTER TABLE `log_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_group`
--
ALTER TABLE `m_group`
  ADD PRIMARY KEY (`id_group`);

--
-- Indexes for table `m_kapal`
--
ALTER TABLE `m_kapal`
  ADD PRIMARY KEY (`id_kapal`);

--
-- Indexes for table `m_kegiatan`
--
ALTER TABLE `m_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`) USING BTREE;

--
-- Indexes for table `m_lokasi`
--
ALTER TABLE `m_lokasi`
  ADD PRIMARY KEY (`id_lokasi`) USING BTREE;

--
-- Indexes for table `m_perusahaan`
--
ALTER TABLE `m_perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`) USING BTREE;

--
-- Indexes for table `t_commercial`
--
ALTER TABLE `t_commercial`
  ADD PRIMARY KEY (`id_commercial`) USING BTREE,
  ADD KEY `id_perusahaan_fk1` (`id_perusahaan`);

--
-- Indexes for table `t_commercial_kegiatan`
--
ALTER TABLE `t_commercial_kegiatan`
  ADD PRIMARY KEY (`id_commercial_kegiatan`),
  ADD KEY `commercial_id_fk` (`id_commercial`) USING HASH;

--
-- Indexes for table `t_invoice`
--
ALTER TABLE `t_invoice`
  ADD PRIMARY KEY (`id_invoice`);

--
-- Indexes for table `t_invoice_kegiatan`
--
ALTER TABLE `t_invoice_kegiatan`
  ADD PRIMARY KEY (`id_invoice_kegiatan`),
  ADD KEY `invoice_id_fk` (`id_invoice`) USING HASH;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_activity`
--
ALTER TABLE `log_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_group`
--
ALTER TABLE `m_group`
  MODIFY `id_group` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `m_kapal`
--
ALTER TABLE `m_kapal`
  MODIFY `id_kapal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `m_kegiatan`
--
ALTER TABLE `m_kegiatan`
  MODIFY `id_kegiatan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `m_lokasi`
--
ALTER TABLE `m_lokasi`
  MODIFY `id_lokasi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `m_perusahaan`
--
ALTER TABLE `m_perusahaan`
  MODIFY `id_perusahaan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_commercial`
--
ALTER TABLE `t_commercial`
  MODIFY `id_commercial` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=262;

--
-- AUTO_INCREMENT for table `t_commercial_kegiatan`
--
ALTER TABLE `t_commercial_kegiatan`
  MODIFY `id_commercial_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `t_invoice`
--
ALTER TABLE `t_invoice`
  MODIFY `id_invoice` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_invoice_kegiatan`
--
ALTER TABLE `t_invoice_kegiatan`
  MODIFY `id_invoice_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_commercial`
--
ALTER TABLE `t_commercial`
  ADD CONSTRAINT `id_perusahaan_fk1` FOREIGN KEY (`id_perusahaan`) REFERENCES `m_perusahaan` (`id_perusahaan`);

--
-- Constraints for table `t_commercial_kegiatan`
--
ALTER TABLE `t_commercial_kegiatan`
  ADD CONSTRAINT `commercial_id_fk` FOREIGN KEY (`id_commercial`) REFERENCES `t_commercial` (`id_commercial`) ON DELETE CASCADE;

--
-- Constraints for table `t_invoice_kegiatan`
--
ALTER TABLE `t_invoice_kegiatan`
  ADD CONSTRAINT `invoice_id_fk` FOREIGN KEY (`id_invoice`) REFERENCES `t_invoice` (`id_invoice`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
