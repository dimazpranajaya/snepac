-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2020 at 07:54 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snepacco_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_activity`
--

CREATE TABLE `log_activity` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `activity_name` varchar(150) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_group`
--

CREATE TABLE `m_group` (
  `id_group` int(11) NOT NULL,
  `nama_group` varchar(100) NOT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_group`
--

INSERT INTO `m_group` (`id_group`, `nama_group`, `keterangan`) VALUES
(9, 'Shipment', 'Shipment Batam'),
(12, 'Tablig Akbar', 'Tablig Akbar');

-- --------------------------------------------------------

--
-- Table structure for table `m_kapal`
--

CREATE TABLE `m_kapal` (
  `id_kapal` int(5) NOT NULL,
  `id_perusahaan` int(5) DEFAULT NULL,
  `nama_kapal` varchar(255) DEFAULT NULL,
  `imo` varchar(255) DEFAULT NULL,
  `grt` decimal(50,0) DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kapal`
--

INSERT INTO `m_kapal` (`id_kapal`, `id_perusahaan`, `nama_kapal`, `imo`, `grt`, `keterangan`) VALUES
(29, 27, 'Baruna', '1', '11', 'Batam'),
(30, 27, 'Senangin', '2', '22', 'Medan'),
(31, 28, 'Dumaiexpress', '1', '22', '222'),
(32, 28, 'Natali', '2', '3', '11');

-- --------------------------------------------------------

--
-- Table structure for table `m_kegiatan`
--

CREATE TABLE `m_kegiatan` (
  `id_kegiatan` int(5) NOT NULL,
  `id_group` int(10) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `ket_kegiatan` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kegiatan`
--

INSERT INTO `m_kegiatan` (`id_kegiatan`, `id_group`, `nama_kegiatan`, `ket_kegiatan`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(32, 9, 'Zikir Bersama', 'Zikir Bersama', NULL, NULL, NULL, NULL),
(33, 9, 'Makan Bersama', 'Makan ', NULL, NULL, NULL, NULL),
(38, 12, 'Solawat', 'Solawat', NULL, NULL, NULL, NULL),
(39, 12, 'Zikir', 'Zikir', NULL, NULL, NULL, NULL),
(40, 12, 'Ceramah', 'Ceramah', NULL, NULL, NULL, NULL),
(41, 12, 'Makan', 'Makan\r\n', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_lokasi`
--

CREATE TABLE `m_lokasi` (
  `id_lokasi` int(5) NOT NULL,
  `id_perusahaan` int(5) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `ket_lokasi` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_lokasi`
--

INSERT INTO `m_lokasi` (`id_lokasi`, `id_perusahaan`, `nama_lokasi`, `ket_lokasi`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(8, 2, 'Batam', 'batam', '2019-12-02 02:09:10', 1, '2019-12-02 08:18:06', 1),
(9, 2, 'Medan', 'kota medan', '2019-12-02 05:12:26', 1, '2019-12-02 08:18:02', 1),
(10, 2, 'Jakarta', 'Jakarta', '2019-12-02 05:49:06', 1, '2019-12-02 08:17:59', 1),
(15, 3, 'Tanjung Uncang', 'Tanjung Uncang', '2019-12-04 05:23:07', 1, '2019-12-04 05:23:19', 1),
(16, 10, 'Stungkal', 'Stungkal', '2019-12-06 02:43:18', 1, '2019-12-06 02:43:18', 1),
(18, 3, 'Stungkal', 'Stungkal\r\n', '2019-12-06 02:43:54', 1, '2019-12-06 02:43:54', 1),
(20, 1, 'Riau Pekanbaru', 'Riau Pekanbaru', '2019-12-07 12:55:06', 1, '2019-12-07 12:55:06', 1),
(21, 11, 'Tanjung Uncang Batam', 'Tanjung Uncang Batam', '2019-12-27 15:48:30', 1, '2019-12-27 16:29:00', 1),
(22, 11, 'Riau Pekanbaru', '11', '2019-12-27 16:26:46', 1, '2019-12-27 16:26:46', 1),
(25, 21, 'Riau Pekanbaru', 'Riau Pekanbaru', '2020-01-27 22:46:48', 1, '2020-01-27 22:46:48', 1),
(26, 21, 'Medan', 'medamn', '2020-01-27 22:47:19', 1, '2020-01-27 22:47:19', 1),
(27, 24, 'Medan', '1212', '2020-01-28 14:40:16', 1, '2020-01-28 14:40:16', 1),
(28, 27, 'Indonesia', 'Asia', '2020-01-29 14:00:56', 1, '2020-01-29 14:00:56', 1),
(29, 28, 'Natuna', 'batam', '2020-01-29 15:26:17', 1, '2020-01-29 15:26:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_perusahaan`
--

CREATE TABLE `m_perusahaan` (
  `id_perusahaan` int(5) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `nama_owner` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(10) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `nama_kapal` varchar(255) DEFAULT NULL,
  `ket_perusahaan` text DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_perusahaan`
--

INSERT INTO `m_perusahaan` (`id_perusahaan`, `nama_perusahaan`, `nama_owner`, `kode_perusahaan`, `alamat`, `nama_kapal`, `ket_perusahaan`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(27, 'putra nur halim', 'Putra Halim', 'BRN', 'Indonesia', NULL, 'Asia', '2020-01-29 14:00:31', 1, '2020-01-29 14:00:31', 1),
(28, 'Nur Pertama', 'Nurul', 'LB', '12', NULL, '12', '2020-01-29 15:25:43', 1, '2020-01-29 15:25:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_commercial`
--

CREATE TABLE `t_commercial` (
  `id_commercial` int(5) NOT NULL,
  `pda_no` varchar(30) NOT NULL,
  `id_perusahaan` int(10) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `id_lokasi` int(10) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `mata_uang` varchar(5) NOT NULL,
  `keterangan` text NOT NULL,
  `status` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='PDA No adalah Dokumen Commercial, yang terbuat secara otomatis ketika simpan form commercial.\r\n\r\nCth : 025/DW/PDA/2019\r\n[auto_increment]/[kode_perusahaan]/[PDA]/[current_year]';

--
-- Dumping data for table `t_commercial`
--

INSERT INTO `t_commercial` (`id_commercial`, `pda_no`, `id_perusahaan`, `nama_perusahaan`, `id_lokasi`, `nama_lokasi`, `tanggal`, `mata_uang`, `keterangan`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(261, '1/BRN/PDA/2020', 27, 'putra nur halim', 28, 'Indonesia', '2020-01-29 00:00:00', 'IDR', '', 1, '2020-01-29 14:16:31', 1, '2020-01-29 14:33:19', 3),
(262, '262/BRN/PDA/2020', 27, 'putra nur halim', 28, 'Indonesia', '2020-01-29 00:00:00', 'IDR', 'dana tidak ada', 3, '2020-01-29 14:31:25', 3, '2020-01-29 14:35:13', 1),
(263, '263/BRN/PDA/2020', 27, 'putra nur halim', 28, 'Indonesia', '2020-01-29 00:00:00', 'IDR', '', 0, '2020-01-29 15:23:21', 2, '2020-01-29 15:23:21', 2),
(264, '264/BRN/PDA/2020', 27, 'putra nur halim', 28, 'Indonesia', '2020-01-29 00:00:00', 'IDR', '', 0, '2020-01-29 14:54:44', 2, '2020-01-29 14:54:44', 2),
(265, '265/LB/PDA/2020', 28, 'Nur Pertama', 29, 'Natuna', '2020-01-29 00:00:00', 'IDR', '', 1, '2020-01-29 15:26:50', 1, '2020-01-29 15:26:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_commercial_kegiatan`
--

CREATE TABLE `t_commercial_kegiatan` (
  `id_commercial_kegiatan` int(11) NOT NULL,
  `id_commercial` int(11) DEFAULT NULL,
  `id_kegiatan` int(11) DEFAULT NULL,
  `nama_kegiatan` varchar(255) DEFAULT NULL,
  `unit` int(5) DEFAULT NULL,
  `harga` decimal(12,2) DEFAULT NULL,
  `harga_dasar` decimal(12,2) DEFAULT NULL,
  `at_cost` decimal(12,2) DEFAULT NULL,
  `ket_kegiatan` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_commercial_kegiatan`
--

INSERT INTO `t_commercial_kegiatan` (`id_commercial_kegiatan`, `id_commercial`, `id_kegiatan`, `nama_kegiatan`, `unit`, `harga`, `harga_dasar`, `at_cost`, `ket_kegiatan`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(106, 261, 41, 'Makan', 1, '22.00', '112121212.00', '2121212.00', '1', '2020-01-29 14:16:31', 1, '2020-01-29 14:16:31', 1),
(107, 261, 40, 'Ceramah', 2, '2.00', '12121212.00', '33.00', '21', '2020-01-29 14:16:31', 1, '2020-01-29 14:16:31', 1),
(108, 262, 41, 'Makan', 1, '22.00', '12133333.00', '222.00', '21', '2020-01-29 14:31:25', 3, '2020-01-29 14:31:25', 3),
(110, 264, 41, 'Makan', 11, '111.00', '1214444.00', '11.00', '11', '2020-01-29 14:54:44', 2, '2020-01-29 14:54:44', 2),
(112, 263, 41, 'Makan', 1, '22.00', '1215555.00', '44.00', '1', '2020-01-29 15:23:21', 2, '2020-01-29 15:23:21', 2),
(113, 265, 41, 'Makan', 1, '12.00', '1256666.00', '2.00', '1', '2020-01-29 15:26:50', 1, '2020-01-29 15:26:50', 1),
(114, 265, 40, 'Ceramah', 22, '222.00', '1214444.00', '22.00', '22', '2020-01-29 15:26:50', 1, '2020-01-29 15:26:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_invoice`
--

CREATE TABLE `t_invoice` (
  `id_invoice` int(5) NOT NULL,
  `no_invoice` varchar(100) NOT NULL,
  `id_perusahaan` int(5) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `id_lokasi` int(5) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `mata_uang` varchar(15) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `tanggal_kirim` datetime DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `term` varchar(20) DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  `ket` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_invoice`
--

INSERT INTO `t_invoice` (`id_invoice`, `no_invoice`, `id_perusahaan`, `nama_perusahaan`, `id_lokasi`, `nama_lokasi`, `mata_uang`, `keterangan`, `tanggal_kirim`, `tanggal_bayar`, `term`, `status`, `ket`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(90, '1/BRN/INV/01/2020', 27, 'putra nur halim', 28, 'Indonesia', 'IDR', NULL, '2020-01-14 00:00:00', '2020-01-31 00:00:00', '121', 0, '', '2020-01-29 15:24:17', 4, '2020-01-29 15:24:17', 4),
(91, '91/LB/INV/01/2020', 28, 'Nur Pertama', 29, 'Natuna', 'IDR', NULL, '2020-01-07 00:00:00', '2020-02-04 00:00:00', '1212', 1, '', '2020-01-29 15:27:04', 1, '2020-01-29 15:27:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_invoice_kegiatan`
--

CREATE TABLE `t_invoice_kegiatan` (
  `id_invoice_kegiatan` int(11) NOT NULL,
  `id_invoice` int(11) DEFAULT NULL,
  `id_kegiatan` int(11) DEFAULT NULL,
  `nama_kegiatan` varchar(255) DEFAULT NULL,
  `unit` int(5) DEFAULT NULL,
  `harga` decimal(12,2) DEFAULT NULL,
  `at_cost` decimal(12,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_invoice_kegiatan`
--

INSERT INTO `t_invoice_kegiatan` (`id_invoice_kegiatan`, `id_invoice`, `id_kegiatan`, `nama_kegiatan`, `unit`, `harga`, `at_cost`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(119, 90, 41, 'Makan', 1, '22.00', '44.00', '2020-01-29 15:24:17', 4, '2020-01-29 15:24:17', 4),
(120, 90, 40, 'Ceramah', 2, '2.00', '33.00', '2020-01-29 15:24:17', 4, '2020-01-29 15:24:17', 4),
(121, 91, 41, 'Makan', 1, '12.00', '2.00', '2020-01-29 15:27:04', 1, '2020-01-29 15:27:04', 1),
(122, 91, 40, 'Ceramah', 22, '222.00', '22.00', '2020-01-29 15:27:04', 1, '2020-01-29 15:27:04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` enum('0','1','2','3','4','5') NOT NULL,
  `last_login` datetime NOT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `last_login`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', '2019-11-25 18:48:30', 'admin'),
(2, 'staff_co', '21232f297a57a5a743894a0e4a801fc3', '2', '0000-00-00 00:00:00', 'staff commercial'),
(3, 'manager_co', '21232f297a57a5a743894a0e4a801fc3', '3', '0000-00-00 00:00:00', 'manager commercial'),
(4, 'staff_fine', '21232f297a57a5a743894a0e4a801fc3', '4', '0000-00-00 00:00:00', 'staff finance'),
(5, 'manager_fine', '21232f297a57a5a743894a0e4a801fc3', '5', '0000-00-00 00:00:00', 'manager finance');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_activity`
--
ALTER TABLE `log_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_group`
--
ALTER TABLE `m_group`
  ADD PRIMARY KEY (`id_group`);

--
-- Indexes for table `m_kapal`
--
ALTER TABLE `m_kapal`
  ADD PRIMARY KEY (`id_kapal`);

--
-- Indexes for table `m_kegiatan`
--
ALTER TABLE `m_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`) USING BTREE;

--
-- Indexes for table `m_lokasi`
--
ALTER TABLE `m_lokasi`
  ADD PRIMARY KEY (`id_lokasi`) USING BTREE;

--
-- Indexes for table `m_perusahaan`
--
ALTER TABLE `m_perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`) USING BTREE;

--
-- Indexes for table `t_commercial`
--
ALTER TABLE `t_commercial`
  ADD PRIMARY KEY (`id_commercial`) USING BTREE;

--
-- Indexes for table `t_commercial_kegiatan`
--
ALTER TABLE `t_commercial_kegiatan`
  ADD PRIMARY KEY (`id_commercial_kegiatan`),
  ADD KEY `commercial_id_fk` (`id_commercial`) USING HASH;

--
-- Indexes for table `t_invoice`
--
ALTER TABLE `t_invoice`
  ADD PRIMARY KEY (`id_invoice`);

--
-- Indexes for table `t_invoice_kegiatan`
--
ALTER TABLE `t_invoice_kegiatan`
  ADD PRIMARY KEY (`id_invoice_kegiatan`),
  ADD KEY `invoice_id_fk` (`id_invoice`) USING HASH;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_activity`
--
ALTER TABLE `log_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_group`
--
ALTER TABLE `m_group`
  MODIFY `id_group` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `m_kapal`
--
ALTER TABLE `m_kapal`
  MODIFY `id_kapal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `m_kegiatan`
--
ALTER TABLE `m_kegiatan`
  MODIFY `id_kegiatan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `m_lokasi`
--
ALTER TABLE `m_lokasi`
  MODIFY `id_lokasi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `m_perusahaan`
--
ALTER TABLE `m_perusahaan`
  MODIFY `id_perusahaan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `t_commercial`
--
ALTER TABLE `t_commercial`
  MODIFY `id_commercial` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT for table `t_commercial_kegiatan`
--
ALTER TABLE `t_commercial_kegiatan`
  MODIFY `id_commercial_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `t_invoice`
--
ALTER TABLE `t_invoice`
  MODIFY `id_invoice` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `t_invoice_kegiatan`
--
ALTER TABLE `t_invoice_kegiatan`
  MODIFY `id_invoice_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_commercial_kegiatan`
--
ALTER TABLE `t_commercial_kegiatan`
  ADD CONSTRAINT `commercial_id_fk` FOREIGN KEY (`id_commercial`) REFERENCES `t_commercial` (`id_commercial`) ON DELETE CASCADE;

--
-- Constraints for table `t_invoice_kegiatan`
--
ALTER TABLE `t_invoice_kegiatan`
  ADD CONSTRAINT `invoice_id_fk` FOREIGN KEY (`id_invoice`) REFERENCES `t_invoice` (`id_invoice`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
